<?php

/**
 * classe UsuarioAPI : Esta classe trabalha com a API e o Objeto Usuario
 * @author Arnour Sabino
 *
 */
class UsuarioAPI
{

  /**
   * getUsuario : Busca o Usuario a partir do StUsuario
   * @param $StUsuario
   * @return Usuario
   */
    public static function getUsuario($StUsuario)
    {
        $ArUsuario = PC_UsuarioColetar($StUsuario);

        if (is_pc_error($ArUsuario)) {
            return $ArUsuario;
        }

        $IDUsuario = $ArUsuario['IDUsuario'];

        if (empty($ArUsuario)) {
            return false;
        }

        $ArLayout = false;

        if ($ArUsuario['IDUsuarioMaster']) {
            $IDUsuario = $ArUsuario['IDUsuarioMaster'];
        }

        $ArLayout = PC_UsuarioLayoutColetar($IDUsuario);

        if (!$ArLayout || is_pc_error($ArLayout) || empty($ArLayout)) {
            $ArLayout = false;
        }

        $ArUsuario['Layout'] = $ArLayout;

        $usuario = new Usuario();
        foreach ($ArUsuario as $StPropriedade => $StValor) {
            if (method_exists($usuario, "set$StPropriedade")) {
                call_user_func(array($usuario, "set$StPropriedade"), $StValor);
            }
        }

        return $usuario;
    }


    /**
     * getAvisos : Carrega a lista de Avisos do Usuario
     * @param Usuario $usuario
     * @return array
     */
    public static function getAvisos(Usuario $usuario)
    {
        $ArBuscaAviso = array();

        $ArBuscaAviso['IDUsuario'] = $usuario->getIDUsuario();

        $PC_AvisoColetar = PC_AvisoColetar($ArBuscaAviso, 'USUARIO');

        $ArAvisoUsuario = array();

        foreach ($PC_AvisoColetar as $ArAviso) {
            #Cria o Objeto Aviso
            $Aviso = new Aviso();

            #Carrega o Objeto com os valores
            foreach ($ArAviso as $StPropriedade => $StValor) {
                if (method_exists($Aviso, "set$StPropriedade")) {
                    call_user_func(array($Aviso, "set$StPropriedade"), $StValor);
                }
            }

            #Adiciona o Aviso ao Usuario
            $ArAvisoUsuario[] = $Aviso;
        }

        return $ArAvisoUsuario;
    }

    /**
     * Adiciona um aviso ao Usuario
     * @param Usuario $usuario
     * @param Aviso $aviso
     * @return IDAviso
     */
    public static function avisoAdicionar(Usuario $usuario, Aviso $aviso)
    {
        $aviso->setIDSisCliente($usuario->getIDSisCliente());

        #Adiciona o Aviso ao Banco de Avisos
        $IDAviso = AvisoAPI::avisoAdicionar($aviso);

        #Atribui visualizacao do Aviso
        return PC_AvisoParaUsuario($IDAviso, $usuario->getIDUsuario());
    }


    /**
     * TODO Substituir API DadosDominio. E necessario carregar somente dominios Ativos
     * getDominios : Carrega a lista de Dominios do Usuario
     *
     * @param Usuario $usuario
     * @param array $ArDominio Lista de dominios que deseja-se carregar.
     * Se esta for passada, nao buscara do BD. StDominio e' obrigatorio.
     * @param $Bo30Dias [Deveria ficar no dominio, mas fica no cliente, entao por enquanto o valor vem como parametro]
     * @return array de DominioServico
     */
    public static function getDominios(Usuario &$usuario, $ArDadosDominio = array(), $Bo30Dias = false, $BoMigracao = false)
    {
        $ArDominioServico = array();

        if (empty($ArDadosDominio)) {
            $PC_DominiosUsuario = PC_DominiosUsuario($usuario->getIDUsuario());
        } else {
            $PC_DominoisUsuario[] = $ArDadosDominio;
        }

        $ArDominioNivel = array();
        foreach ($PC_DominiosUsuario as $ArDominio) {
            if (isset($ArDominio['StDominio']) && isset($ArDominio['EnNivel'])) {
                $ArDominioNivel[$ArDominio['StDominio']] = $ArDominio['EnNivel'];
            }
            
            $DominioServico = new DominioServico();

            #COMPATIBILIDADE
            if (is_array($ArDominio)) {
                $DadosDominio = CL_DominioListar($ArDominio['StDominio'], 'ATIVO');
            } else {
                $DadosDominio = CL_DominioListar($ArDominio, 'ATIVO');
            }
            if (!empty($DadosDominio)) {
                if (is_pc_error($DadosDominio)) {
                    mata("N&atilde;o foi poss&iacute;vel verificar os dados do seu plano, por favor, entre em contato com o suporte.<br><a href='logout'>Clique Aqui</a> para logar novamente.");
                }

                $DadosDominio = $DadosDominio[0];
                $DadosDominio['IDSisPlano'] = $DadosDominio['IDPlano'];
                $DadosDominio['IDPlanoCategoria'] = $DadosDominio['IDCategoria'];
                $DadosDominio['StPlanoCategoria'] = $DadosDominio['StCategoria'];
                unset($DadosDominio['IDPlano']);

                $DadosPlano = PC_PlanoDadosListar($DadosDominio['IDSisPlano']);

                // Essa condicao verifica se o plano existe, se ele não existir não deixa o usuário usar o painel
                // Referente ao topico 14683
                if (empty($DadosPlano)) {
                    mata("N&atilde;o foi poss&iacute;vel verificar os dados do seu plano, por favor, entre em contato com o suporte.<br><a href='logout'>Clique Aqui</a> para logar novamente.");
                }

                $DadosDominio['IDPlano'] = $DadosPlano[0]['IDPlano'];

                $DadosDominio['IDSisCliente'] = $DadosDominio['IDCliente'];
                unset($DadosDominio['IDCliente']);

                #Define Conta
                if ($DadosDominio) {
                    $StConta = str_replace(array('Plano ', '(', ')'), '', $DadosDominio['StPlano']);
                    $DominioServico->setStConta(strtoupper($StConta));
                }

                unset($DadosDominio['IDDominio']);

                #Status em Relacao Pagamento
                #Carrega Dados do DominioServico [Dados Basicos da Coleta]

                foreach (array_merge($ArDominio, $DadosDominio) as $StPropriedade => $StValor) {
                    
                    // Topico #368742 - Verificacao para evitar valor fora dos permitidos ADMINISTRADOR|TECNICO
                    // Setar nivel invalido impede de verificar:
                    // se nivel de usuario concede acesso 'a opcao ou grupo de opcao #337126
                    if ($StPropriedade == 'EnNivel' && !in_array($StValor, ['ADMINISTRADOR', 'TECNICO'])) {
                        if (!empty($ArDominio) && isset($ArDominio['EnNivel'])) {
                            $StValor = $ArDominio['EnNivel'];
                        }
                    }
                    
                    if (method_exists($DominioServico, "set$StPropriedade")) {
                        call_user_func(array($DominioServico, "set$StPropriedade"), $StValor);
                    }
                }




                if (!$BoMigracao) {
                    #carrega descontos ativos para o dominio
                    $ArDescontos = FU_DescontoListar(0, 0, $DominioServico->getIDSisDominio(), 'ATIVO');

                    if (!is_pc_error($ArDescontos)) {
                        if ($ArDescontos['ItTotal'] > 0) {
                            $DominioServico->setDeValorDesconto($ArDescontos['ArDados'][0]['DeDesconto']);
                            $DominioServico->setStTipoDesconto($ArDescontos['ArDados'][0]['EnTipoDesconto']);
                            $DominioServico->setDtValidadeDesconto($ArDescontos['ArDados'][0]['DtExpiracao']);
                        }
                    }
                }
                #Planos MINI, PLUS, RUBY, REGISTRO[Util para calculo de quota, e servicos adicionais]
                if ($DominioServico->getIDSisPlano() >= 55) {
                    $DominioServico->setBoPlanoNovo(true);
                }

                if ($DadosDominio['EnTipoDominio'] == 'CONTA_PRINCIPAL') {
                    $DominioServico->setBoCliente(1);
                } else {
                    if ($DadosDominio['EnTipo'] == 'CLIENTE') {
                        $DominioServico->setBoCliente(1);
                    } else {
                        $DominioServico->setBoCliente(0);
                    }
                }

                #Seta Usuario
                $DominioServico->setUsuario($usuario);

                #30DIAS
                $DominioServico->setBo30Dias($Bo30Dias);

                if (!is_pc_error(DominioServicoAPI::getServicoHospedagem($DominioServico))) {
                    $DominioServico->getServicos()->set('ServicoHospedagem', DominioServicoAPI::getServicoHospedagem($DominioServico));
                }

                $DominioServico->getServicos()->set('ServicoRedirect', DominioServicoAPI::getServicoRedirect($DominioServico));

                #DNSs
                $DominioServico->setArDNS(DNS_GetServidorDNS($DominioServico->getStDominio()));

                $DominioServico->setStPeriodicidade($DadosDominio['StPeriodicidade']);
                $DominioServico->setEnPeriodicidadeTipo($DadosDominio['EnPeriodicidadeTipo']);

                $DominioServico->setDtUltDebito($DadosDominio['DtUltDebito']);

                $ArDominioServico[] = $DominioServico;
            }
        }

        # Atualiza o Tipo do usuário de acordo com o domínio selecionado.
        # Alteração para permitir que um usuário principal de uma conta seja secundário em outra

        if (isset($ArDadosDominio['EnTipoUsuario'])) {
            $usuario->setEnTipo($ArDadosDominio['EnTipoUsuario']);
        }
        
        // Topico #368742 - Verificacao para evitar valor fora dos permitidos ADMINISTRADOR|TECNICO
        // EnNivel associado ao dominio era carregado normalmente como ADMINISTRADOR|TECNICO pela PC_DominiosUsuario, 
        // mas chegava como '' na controller.menu.lateral $Dominio->getEnNivel()
        TSessao::setValor('ArDominioNivel', $ArDominioNivel);

        return $ArDominioServico;
    }

    /**
     * Configura o tipo do usuário de acordo com sua relação com o domínio selecionado
     * @param Usuario $usuario
     * @param String  $StDominio
     *
     * @return boolean
     *
     * @author Henrique Rodrigues <henrique@hostnet.com.br>
     */
    public static function setEnTipo(Usuario &$usuario, $StDominio)
    {
        $EnTipo = PC_ColetarTipoUsuario($usuario->getIDUsuario(), $StDominio);

        if (is_pc_error($EnTipo)) {
            return $EnTipo;
        }

        $usuario->setEnTipo($EnTipo);

        return true;
    }


    /**
     * Retorna as contas Adicionais do Cliente [Dominios de Franquia]
     * @param Usuario $usuario
     * @return array de DominioServico
     */
    public function getDominiosFranquia(Usuario &$usuario)
    {
        $ArDominioFranquia = CL_DominioPorIDListar($usuario->getIDSisCliente(), 0, 'CONTA_ADICIONAL', 'ATIVO');

        $ArDominios = array();

        foreach ($ArDominioFranquia as $Dominio) {
            $ArDominios[] = array('StDominio' => $Dominio['StDominio'], 'IDSisDominio' => $Dominio['IDDominio']);
        }

        return self::getDominios($usuario, $ArDominios);
    }


    /**
     * getPerfilOpcoes : Carrega a lista de opcoes que o Usuario/DominioServico possuem
     * @param Usuario $usuario
     * @param DominioServico $dominio
     * @return array contendo grupo
     */
    public static function getPerfilOpcoes(Usuario $usuario, DominioServico $dominio = null, $IDUsuarioTipo = '')
    {
        $ArPerfil = array();

        $IDDominio = $IDTipoServico = $IDSisPlano = $StOpcaoTipo = $StPlano = '';
        $EnNivel = 'TECNICO';

        if ($dominio != null) {
            $IDDominio = $dominio->getIDDominio();
            $IDTipoServico = $dominio->getIDTipoServico();
            $IDSisPlano = $dominio->getIDSisPlano();
            $EnNivel = $dominio->getEnNivel();
        }

        $ArDadosAcessoInterno = TSessao::getValor('acesso');

        $PC_PerfilOpcoes = PC_PerfilOpcoes($usuario->getIDUsuario(), $IDDominio, $IDTipoServico, $usuario->getCliente()->getEntipo(), $IDSisPlano, $IDUsuarioTipo);

        if (empty($PC_PerfilOpcoes)) {
            throw new ViewException('Erro ao coletar op&ccedil;&otilde;es do painel de controle.');
        }

        $PC_GrupoOpcoes = $PC_PerfilOpcoes['Opcoes'];
        $PC_SubOpcoes = $PC_PerfilOpcoes['SubOpcoes'];

        $BoVerificarNivel = false;
        # Verifica se dominio nao corresponde ao usuario, atraves de IDSisCliente
        if (!empty($dominio) && $dominio->getIDSisCliente() != $usuario->getIDSisCliente()) {
            $BoVerificarNivel = true;
        }
        # Verifica se nao eh Master
        elseif ($usuario->isSecundario()) {
            $BoVerificarNivel = true;
        }

        foreach ($PC_GrupoOpcoes as $StGrupo => $ArGrupo) {
            # Conta sem site.
            if (empty($dominio) and $ArGrupo['EnRestricao'] == 'CONTA_COM_SITE') {
                continue;
            }

            if ($BoVerificarNivel) {
                if (!self::nivelPossuiOpcao($EnNivel, $PC_GrupoOpcoes[$StGrupo]['IDOpcaoGrupo'], null)) {
                    continue;
                }
            }

            # Cria o Objeto GRUPO
            $ObjGrupo = new OpcaoGrupo(Encode::forceUTF8($ArGrupo['StOpcaoGrupo']), Encode::forceUTF8($ArGrupo['TxDescricao']), $ArGrupo['EnAtivo'], $ArGrupo['StOpcaoGrupo']);

            $ObjGrupo->setIDOpcaoGrupo($ArGrupo['IDOpcaoGrupo']);
            $ObjGrupo->setStArquivo($ArGrupo['StArquivo']);

            foreach ($ArGrupo['ArOpcao'] as $StOpcao => $ArOpcao) {
                # Conta sem site.
                if (empty($dominio) and $ArOpcao['EnRestricao'] == 'CONTA_COM_SITE') {
                    # 20160802 - REMOVER
                    # Permissão temporária para o usuário jr79096817@gmail.com acessar a opção de redirecionamentos mesmo sem ter sites na conta
                    # Remover esta condição assim que a olução definitiva for aplicada para todos os clientes.
                    if (!($ArOpcao['IDOpcao'] == 37 and $usuario->getIDUsuario() == 83172)) {
                        continue;
                    }
                }

                # Plano Registro de Dominio 100br
                if ($ArOpcao['IDOpcao'] == 102 and $IDSisPlano != 112) {
                    continue;
                }

                if ($BoVerificarNivel) {
                    if (!self::nivelPossuiOpcao($EnNivel, null, $ArOpcao['IDOpcao'])) {
                        continue;
                    }
                }

                #
                # Automacao de Marketing
                # Alem dos representantes com o credenciamento necessario, concede:
                # acesso interno (operadores da Hostnet logados pela intranet)
                $BoRepresentante = $usuario->getIDUsuarioTipo() == 2 || $usuario->getIDUsuarioTipo() == 4;
                $BoOperador = $ArDadosAcessoInterno['BoAcessoInterno'] == 1;
                if ($ArOpcao['IDOpcao'] == 115 && !$BoOperador && !$BoRepresentante) {
                    continue;
                } elseif ($ArOpcao['IDOpcao'] == 115 && ($usuario->getIDUsuarioTipo() == 2 || $usuario->getIDUsuarioTipo() == 3)) {
                    continue;
                }

                # Cria o Objeto OPCAO
                $ObjOpcao = new Opcao(
                 Encode::forceUTF8($ArOpcao['StOpcao']),
                    $ArOpcao['StArquivo'],
                    $ArOpcao['EnAtivo'],
                 $ArOpcao['EnBeta'],
                    Encode::forceUTF8($ArOpcao['TxDescricao']),
                    $ArOpcao['EnTarget'],
                    $ArOpcao['SetTipo']
                );

                foreach ($PC_SubOpcoes as $ArSubOpcoes) {
                    if ($ArSubOpcoes['IDOpcao'] == $ArOpcao['IDOpcao']) {
                        # Cria o Objeto SUBOPCAO
                        $ObjSubOpcao = new SubOpcao(
                        $ArSubOpcoes['IDSubOpcao'],
                            $ArSubOpcoes['IDOpcao'],
                            $ArSubOpcoes['StSubOpcao'],
                            $ArSubOpcoes['StArquivo'],
                        $ArSubOpcoes['EnAtivo'],
                            $ArSubOpcoes['EnBeta'],
                            $ArSubOpcoes['SetTipo']
                        );

                        $ObjOpcao->getArSubOpcao()->add($ObjSubOpcao);
                    }
                }

                $ObjOpcao->setIDOpcao($ArOpcao['IDOpcao']);

                # Adiciona Opcao ao Grupo
                $ObjGrupo->getArOpcao()->add($ObjOpcao);
            }

            $ArPerfil[] = $ObjGrupo;
        }

        return $ArPerfil;
    }

    /**
     * nivelPossuiOpcao : Indica se nivel de usuario concede acesso 'a opcao ou grupo de opcao #337126.
     *
     * @param $EnNivel
     * @param $IDGrupoOpcao
     * @param $IDOpcao
     * @return boolean
     */
    public static function nivelPossuiOpcao($EnNivel, $IDGrupoOpcao = null, $IDOpcao = null)
    {
        # Verifica se passou Grupo OU Opcao
        if (!$IDGrupoOpcao && !$IDOpcao) {
            return false;
        } elseif (!in_array($EnNivel, ['ADMINISTRADOR', 'TECNICO'])) {
            
            // Topico #368742 - Tratamento para lidar com parametro EnNivel fora dos permitidos ADMINISTRADOR|TECNICO
            // Tenta recuperar da sessao
            $ArDominioNivel = TSessao::getValor('ArDominioNivel');
            $StDominio = TSessao::getValor('StDominio');
            if(isset($ArDominioNivel[$StDominio])){
                if(in_array($ArDominioNivel[$StDominio], ['ADMINISTRADOR', 'TECNICO'])) {
                    $EnNivel = $ArDominioNivel[$StDominio];
                }
            }
        }
        
        if (!in_array($EnNivel, ['ADMINISTRADOR', 'TECNICO'])) {
            return false;
        }

        # Define grupos/opcoes indisponiveis a um nivel de usuario
        $ArGrupoOpcoesTecnico = [
            // Financeiro
            2,
            // Controle de Franquia, solicitado no Topico #367191 (#367067, #367192)
            14
        ];

        # Restringe o acesso de usuarios secundarios de nivel tecnico a importantes opcoes administrativas.
        # Alterado por Thiago Nunes em 2018-08-20.
        //$ArOpcoesTecnico = [
        //    // Conta (usuarios, ouvidoria, cadastro)
        //    63, 4, 2,
        //    // Financeiro
        //    /*82, 83, 6, 84, 54, 67, 85,*/
        //    // Site (redirecionamentos)
        //    37,
        //];
        $ArOpcoesTecnico = [
            # Conta
            2,   // Cadastro
            4,   // Ouvidoria
            5,   // Cancelamento de site
            29,  // Plano do site
            30,  // Troca de nome do site
            56,  // Parcerias e promocoes
            63,  // Usuarios
            66,  // Revenda
            110, // Credenciado Hostnet
            115, // Automacao de Marketing
            110, // Controle de Franquia, solicitado no Topico #367191 (#367067, #367192)
            121, // Controle de Franquia, solicitado no Topico #367191 (#367067, #367192)

            # Site (redirecionamentos)
            37
        ];

        if ($EnNivel == 'TECNICO') {
            if ($IDGrupoOpcao && in_array($IDGrupoOpcao, $ArGrupoOpcoesTecnico)) {
                return false;
            } elseif ($IDOpcao && in_array($IDOpcao, $ArOpcoesTecnico)) {
                return false;
            }
        } elseif ($EnNivel == 'ADMINISTRADOR') {
            return true;
        }

        return true;
    }


    public static function getGrupoOpcao()
    {
    }

    /**
     * Nega acesso a uma opcao para Usuario/Dominio
     * @param $IDUsuario
     * @param $IDOpcao
     * @param $IDDominio
     * @return bool
     * @deprecated Tabelas UsuarioDoesntHaveOpcao e UsuarioDoesntHaveOpcaoAll descontinuadas em 2018-04-24 por Thiago Nunes.
     */
    //public static function opcaoRemover($IDUsuario, $ArOpcao, $IDDominio = 0)
    //{
    //    return PC_OpcaoProibidaUsuario($IDUsuario, $ArOpcao, $IDDominio);
    //}

    /**
     * Retira 'negacao' de acesso a uma opcao
     * @param $IDUsuario
     * @param $ArOpcao
     * @param $IDDominio
     * @return bool
     * @deprecated Tabelas UsuarioDoesntHaveOpcao e UsuarioDoesntHaveOpcaoAll descontinuadas em 2018-04-24 por Thiago Nunes.
     */
    //public static function opcaoAdicionar($IDUsuario, $ArOpcao, $IDDominio = 0)
    //{
    //    return PC_OpcaoPermitidaUsuario($IDUsuario, $ArOpcao, $IDDominio);
    //}

    /**
     * alterarSenha : Altera a senha do Usuario
     * @param Usuario $usuario
     * @return void
     */
    public static function alterarSenha(Usuario &$usuario, $BoUsuarioAdicional = 0)
    {
        $ArDadosUsuario = array();
        $ArDadosCliente['StSenha'] = $usuario->getStSenha();

        $PC_UsuarioEditar = PC_UsuarioEditar(
            $usuario->getIDUsuario(),
            $ArDadosCliente
        );

        # Atualiza o Objeto a menos que seja um usuario adicional
        if (!$BoUsuarioAdicional) {
            $usuario = self::getUsuario($usuario->getStUsuario());
        }

        return $PC_UsuarioEditar;
    }

    public static function alterarUsuario(Usuario &$usuario, array $dados = [])
    {
        $PC_UsuarioEditar = PC_UsuarioEditar($usuario->getIDUsuario(), $dados);

        return $PC_UsuarioEditar;
    }


    public static function getUsuariosAdicionais(Usuario &$usuario, $ArDominios = [])
    {
        $ArDominiosCliente = CL_DominioPorIDListar($usuario->getIDSisCliente(), 'ATIVO', 0, 'ATIVO');
        if (empty($ArDominios)) {
            foreach ($ArDominiosCliente as $ArDominio) {
                $ArDominios[] = $ArDominio['StDominio'];
            }
        }

        $ArUsuarioAdicional = array();

        $PC_UsuarioListar = PC_UsuarioListar($usuario->getIDSisCliente(), $ArDominios);

        foreach ($PC_UsuarioListar as $ArUsuario) {
            $UsuarioAdicional = new Usuario('', '', '', '', '', 0, 0, $ArUsuario['IDSisCliente'], 1);

            $ArUsuario['IDUsuarioMaster'] = $usuario->getIDUsuario();

            foreach ($ArUsuario as $StPropriedade => $StValor) {
                if (method_exists($UsuarioAdicional, "set$StPropriedade")) {
                    call_user_func(array($UsuarioAdicional, "set$StPropriedade"), $StValor);
                }
            }

            $ArUsuarioAdicional[] = $UsuarioAdicional;
        }

        return $ArUsuarioAdicional;
    }

    public static function getArDominiosUsuario($IDUsuario)
    {
        return PC_DominiosUsuario($IDUsuario);
    }

    public static function adicionarDominio($IDDominio, $IDUsuario, $EnTipoUsuario = 'PRI', $EnNivel = 'TECNICO')
    {
        return PC_DominioParaUsuario($IDDominio, $IDUsuario, $EnTipoUsuario, $EnNivel);
    }

    public static function removerDominio($IDDominio, $IDUsuario)
    {
        return PC_DominioUsuarioRemover($IDDominio, $IDUsuario);
    }

    /**
     * Remove um usuário ou a relação de um usuário secundário com domínios deste painel
     * @param  Usuario  $Usuario
     * @param  int      $IDUsuarioPrincipal
     *
     * @return bool|pc_error
     *
     * @author Henrique Rodrigues <henrique@hostnet.com.br>
     */
    public static function removerUsuario(Usuario $Usuario, $IDUsuarioRemover)
    {

        # Coleta as informaçõs sobre o usuário a se removido
        $ArUsuario = PC_UsuarioColetar($IDUsuarioRemover, true);

        # se ocorrer erro, retorna para a controller
        if (is_pc_error($ArUsuario)) {
            return $ArUsuario;
        }

        # remover as conexões com os domínios de mesmo IDCliente do usuáiro principal.
        $RemoverAcesso = PC_RemoverAcessoDominios($Usuario->getIDSisCliente(), $IDUsuarioRemover);

        if (is_pc_error($RemoverAcesso)) {
            return $RemoverAcesso;
        }

        # Se o usuáiro for um usuáiro secundário criado neste painel, removê-lo totalmente e retornar o resultado para a view
        if ($ArUsuario['IDSisCliente'] == $Usuario->getIDSisCliente()) {
            PC_UsuarioRemover($IDUsuarioRemover);
        }

        return true;
    }

    public static function usuarioExiste($Stusuario)
    {
        $ArUsuarioInfo = PC_UsuarioColetar($Stusuario);

        if (!empty($ArUsuarioInfo)) {
            return true;
        }

        return false;
    }

    public static function usuarioColetar($Stusuario)
    {
        return PC_UsuarioColetar($Stusuario);
    }

    public static function usuarioAdicionar($StUsuario, $StSenha, $HaDados, $IDTipoUsuario = 0, $IDUsuarioMaster = 0, $IDSisCliente = 0, $EnTipo = 0)
    {
        return PC_UsuarioAdicionar($StUsuario, $StSenha, $IDTipoUsuario, $IDUsuarioMaster, $IDSisCliente, $HaDados, $EnTipo);
    }

    public static function getOpcoesUsuario($IDUsuario, $IDDominio, $StOpcaoTipo, $IDUsuarioTipo = '')
    {
        return PC_PerfilOpcoes($IDUsuario, $IDDominio, '', $StOpcaoTipo, '', $IDUsuarioTipo);
    }

    public static function editarLayout($IDUsuario, $TxHTMLTopo = '', $TxHTMLBase = '', $StFundoCor = '', $EnSec = 'TRUE')
    {
        return PC_LayoutEditar($IDUsuario, $TxHTMLTopo, $TxHTMLBase, $StFundoCor, $EnSec);
    }

    public static function removerLayout($IDUsuario)
    {
        return PC_LayoutRemover($IDUsuario);
    }

    public static function getListaStDominios(Usuario $usuario, $BlDadosCompletos = 0)
    {
        $ArDominios = PC_DominiosUsuario($usuario->getIDUsuario());

        $ArDados = CL_CadastroListar($usuario->getCliente()->getIDCliente(), array(), array(), array(), array(), array(), 0, 0, 'CONTA_PRINCIPAL');

        # Comentado por Thiago Nunes em 2016-10-10.
        //if ($usuario->getIDUsuarioTipo() == 2) {

        //Se for franquia bota o dominio principal como primeiro no painel
        $ArDominioPrincipal = [];

        foreach ($ArDominios as $index => $ArDominio) {
            if (isset($ArDominio['StDominio'])
                && isset($ArDados['StDominio'])
                && $ArDominio['StDominio'] == $ArDados['StDominio']
            ) {
                $ArDominioPrincipal = $ArDominio;
                unset($ArDominios[$index]);
                break;
            }
        }

        if (!empty($ArDominioPrincipal)) {
            array_unshift($ArDominios, $ArDominioPrincipal);
        }

        //}
        if ($BlDadosCompletos == 1) {
            return $ArDominios;
        }

        $return = array();

        foreach ($ArDominios as $ArDominio) {
            $return[] = trim($ArDominio['StDominio']);
        }

        if (! empty($return)) {
            if (empty(CL_DominioPorIDListar($usuario->getCliente()->getIDCliente(), 'ATIVO', 0, 'ATIVO'))) {
                $return[] = $usuario->getStUsuario();
            }
        }

        return $return;
    }

    public static function getListaDominios(Usuario $usuario, $IDCliente = 0)
    {
        $ArDominios = PC_DominiosUsuario($usuario->getIDUsuario());

        # Adicionado por Thiago Nunes em 2016-10-10. Bloqueio de acesso ao Painel. Topico #246865.
        foreach ($ArDominios as $index => $ArDominio) {
            if ($IDCliente && $IDCliente != CL_PegaIDCliente($ArDominio['StDominio'])) {
                unset($ArDominios[$index]);
            } else {
                $ArDominios[$index]['IDSisCliente'] = CL_PegaIDCliente($ArDominio['StDominio']);
            }
        }

        $ArDados = CL_CadastroListar(
            $usuario->getIDSisCliente(),
            [],
            [],
            [],
            [],
            [],
            0,
            0,
            'CONTA_PRINCIPAL'
        );

        //Se for franquia bota o dominio principal como primeiro no painel
        $ArDominioPrincipal = array();

        foreach ($ArDominios as $index => $ArDominio) {
            if (isset($ArDominio['StDominio'])
                && isset($ArDados['StDominio'])
                && $ArDominio['StDominio'] == $ArDados['StDominio']
            ) {
                $ArDominioPrincipal = $ArDominio;
                unset($ArDominios[$index]);
                break;
            }
        }

        if (!empty($ArDominioPrincipal)) {
            array_unshift($ArDominios, $ArDominioPrincipal);
        }

        if (! empty($ArDominios)) {
            if (empty(CL_DominioPorIDListar($usuario->getIDSisCliente(), 'ATIVO', 0, 'ATIVO'))) {
                $ArDominios[] = [
                    'IDDominio' => 0,
                    'StDominio' => $usuario->getStUsuario(),
                    'IDSisDominio' => 0,
                    'IDTipoServico' => 0,
                    'EnTipoUsuario' => '',
                    'StSenha' => '',
                    'IDSisCliente' => 0
                ];
            }
        }

        return $ArDominios;
    }

    public static function getListaClientesHelpDesk(Usuario $usuario)
    {
        return PC_DominiosUsuarioHelpDesk($usuario->getIDUsuario());
    }

    public static function AcessoSmsInserir($IDUsuario, $StChave)
    {
        return PC_AcessoSmsInserir($IDUsuario, $StChave);
    }

    public static function AcessoSmsPesquisar($IDUsuario)
    {
        return PC_AcessoSmsPesquisar($IDUsuario);
    }

    public static function AcessoSmsEditar($IDUsuario)
    {
        return PC_AcessoSmsEditar($IDUsuario);
    }

    public static function getSitesSSL($IDUsuario)
    {
        return SSL_ColetarSites($IDUsuario);
    }

    public static function getCertificadosSSL($IDUsuario, $BoAtivo = false, $BoWildcard = false)
    {
        return SSL_ColetarCertificados($IDUsuario, $BoAtivo, $BoWildcard);
    }

    public static function gerarCertificadoCSR($ArDados, $IDCertificadoCSR = 0, $StDominio = '')
    {
        return SSL_GerarCertificadoCSR($ArDados, $IDCertificadoCSR, $StDominio);
    }

    public static function insereChaveSSLBanco($StChave, $StDominio) {
        return SSL_InsereChaveSSLBanco($StChave, $StDominio);
    }

    public static function criarCertificado($IDCertificadoCSR, $IDUsuario, $EnOrigemCertificado)
    {
        return SSL_CriarCertificado($IDCertificadoCSR, $IDUsuario, $EnOrigemCertificado);
    }

    public static function insereCertificadoSSL($IDCertificadoCSR, $IDUsuario, $EnOrigemCertificado, $TxCertificado)
    {
        return SSL_InsereCertificado($IDCertificadoCSR, $IDUsuario, $EnOrigemCertificado, $TxCertificado);
    }

    public static function criarSiteSSL($StURL, $IDUsuario)
    {
        return SSL_CriarSiteSSL($StURL, $IDUsuario);
    }

    public static function vincularSite($IDSite, $IDCertificado)
    {
        return SSL_VincularSite($IDSite, $IDCertificado);
    }

    public static function configurarCertificado($IDCertificado, $TxCertificado)
    {
        return SSL_ConfigurarCertificado($IDCertificado, $TxCertificado);
    }

    public static function getDadosCertificado($IDCertificado)
    {
        return SSL_ColetarDadosCertificado($IDCertificado);
    }

    public static function configurarSite($StSite, $IDCertificado, $IDUsuario)
    {
        return SSL_ConfigurarSite($StSite, $IDCertificado, $IDUsuario);
    }

    public static function desativarCertificado($IDCertificado)
    {
        return SSL_DesativarCertificado($IDCertificado);
    }

    public static function removerCertificado($IDCertificado)
    {
        return SSL_RemoverCertificado($IDCertificado);
    }

    public static function getDadosCertificadoCSR($IDCertificadoCSR)
    {
        return SSL_ColetarCertificadoCSR($IDCertificadoCSR);
    }

    /**
     *
     * @param Usuario $Usuario - Usuário que será verificado
     *
     * @return boolean
     */
    public static function hasPermissaoCobranca(Usuario $Usuario, $StOperacao)
    {
        # 72 é a ação adicionada ao log quando é efetuada uma cobrança de cartão de crédito pelo painel de controle.
        $ArLogCobrancas = LogSistema::listar(72, $Usuario->getIDUsuario());

        $BoPermitirCobranca = true;
        $ItLimiteMinutosParaMesmaOperacao = 10;

        # Varre as cobranças do usuário para verificar se, na última hora, ele já efetuou este mesmo procedimento.
        foreach ($ArLogCobrancas as $ArLog) {
            # Obtem os dados da cobrança em  um array. os dados estãoseparados no log por '###'
            $ArDadosObjeto = explode('###', $ArLog['TxDetalhes']);

            # Pega a hora da cobrança.
            $DtHoraLog = date_create($ArLog['DtDataHora']);
            $DtHoraAtual = date_create(date('Y-m-d H:i:s'));
            $DtDiff = date_diff($DtHoraLog, $DtHoraAtual);

            # Verifica se a operação no log não corresponde a desejada ou se não foi concluída no dia atual.
            if ($ArDadosObjeto[0] != $StOperacao || $DtDiff->days > 1) {
                continue;
            }

            # Verifica se o log está trazendo o retorno da mesma hora dez minutos atrás.
            if ($DtDiff->h == 0 && $DtDiff->i < $ItLimiteMinutosParaMesmaOperacao) {
                $BoPermitirCobranca = false;

                break;
            }
        }

        if ($BoPermitirCobranca) {
            return true;
        }

        $ItTempoParaNovaCompra = $ItLimiteMinutosParaMesmaOperacao - $DtDiff->i;

        return new PC_Error('Log', "Uma solicitação de compra para esse item já está sendo processada. Por favor, aguarde mais {$ItTempoParaNovaCompra} minuto(s) para realizar uma nova compra.");
    }

    public static function adicionarDominioPorEmail($StUsuario, $IDDominio, $EnTipo = 'PRI', $EnNivel = 'TECNICO')
    {
        $ArUsuario = PC_UsuarioColetar($StUsuario);

        if (is_pc_error($ArUsuario)) {
            return $ArUsuario;
        }

        return PC_DominioParaUsuario($IDDominio, $ArUsuario['IDUsuario'], $EnTipo, $EnNivel);
    }

    /**
     * Retorna as opcoes bloqueadas para o plano informado.
     *
     * @param $IDPlano
     */
    public static function OpcaoBloqueadaPlanoListar($IDPlano)
    {
        if (!Validar::ID($IDPlano)) {
            return new PC_Error('OPCAO_BLOQUEADA_PLANO_LISTAR', 'O plano informado n&atilde;o &eacute; v&aacute;lido.');
        }

        $dbPainel = Database::PainelDB();

        $StSQL = "
    SELECT
    IDOpcao
    FROM
    PlanoDoesntHaveOpcao
    WHERE
    IDPlano = {$IDPlano}
    ";

        if (!$Query = $dbPainel->query($StSQL)) {
            return new PC_Error('OPCAO_BLOQUEADA_PLANO_LISTAR', "{$dbPainel->errno}: {$dbPainel->error}");
        }

        if ($Query->num_rows == 0) {
            return array();
        }

        $ArOpcaoBloqueada = array();

        while ($ArRow = $Query->fetch_assoc()) {
            $ArOpcaoBloqueada[] = $ArRow['IDOpcao'];
        }

        return $ArOpcaoBloqueada;
    }

    public static function OpcaoBloqueadaUsuarioListar($IDUsuario, $IDDominio = 0)
    {
        if (!Validar::ID($IDUsuario)) {
            return new PC_Error('OPCAO_BLOQUEADA_PLANO_LISTAR', 'O usu&aacute;rio informado n&atilde;o &eacute; v&aacute;lido.');
        }

        if (!Validar::Integer($IDDominio)) {
            return new PC_Error('OPCAO_BLOQUEADA_PLANO_LISTAR', 'O dom&iacute;nio informado n&atilde;o &eacute; v&aacute;lido.');
        }

        $StWhereDominio = Validar::ID($IDDominio) ? " AND IDDominio = $IDDominio " : "";

        $dbPainel = Database::PainelDB();

        $StSQL = "SELECT
    IDOpcao
    FROM
    UsuarioDoesntHaveOpcao
    WHERE
    IDUsuario = {$IDUsuario}
    $StWhereDominio";

        if (!$Query = $dbPainel->query($StSQL)) {
            return new PC_Error('OPCAO_BLOQUEADA_USUARIO_LISTAR', "{$dbPainel->errno}: {$dbPainel->error}");
        }

        if ($Query->num_rows == 0) {
            return array();
        }

        $ArOpcaoBloqueada = array();

        while ($ArRow = $Query->fetch_assoc()) {
            $ArOpcaoBloqueada[] = $ArRow['IDOpcao'];
        }

        return $ArOpcaoBloqueada;
    }

    /**
     * UsuarioDominioFavoritar
     *
     * Marcar o site selecionado como favorito.
     *
     * @param Integer  $IDUsuario
     * @param Integer  $IDDominio
     * @return \PC_Error
     */
    public static function UsuarioDominioFavoritar($IDUsuario, $IDDominio)
    {
        if (!Validar::ID($IDUsuario)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITAR', 'O usuário informado não é válido.');
        }

        if (!Validar::Integer($IDDominio)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITAR', 'O domínio informado não é válido.');
        }

        $dbPainel = Database::PainelDB();

        #
        # Desfavoritar outros domínios primeiro.
        #

        $StSQL = "
    UPDATE
    UsuarioHasDominio
    SET
    EnFavorito = 'FALSE'
    WHERE
    IDUsuario = $IDUsuario
    AND IDDominio <> $IDDominio
    AND EnFavorito = 'TRUE'";

        if (!$dbPainel->query($StSQL)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITAR', "{$dbPainel->errno}: {$dbPainel->error}");
        }


        #
        # Marcar o dominio escolhido como favorito do usuário.
        #

        $StSQL = "
    UPDATE
    UsuarioHasDominio
    SET
    EnFavorito = 'TRUE'
    WHERE
    IDUsuario = $IDUsuario
    AND IDDominio = $IDDominio
    AND EnFavorito = 'FALSE'
    LIMIT 1";

        if (!$dbPainel->query($StSQL)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITAR', "{$dbPainel->errno}: {$dbPainel->error}");
        }
    }

    /**
     * UsuarioDominioFavoritoListar
     *
     * Retorna o domínio favorito do usuário.
     * @param Integer $IDUsuario
     *
     * @return Array
     */
    public static function UsuarioDominioFavoritoListar($IDUsuario)
    {
        if (!Validar::ID($IDUsuario)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITO_LISTAR', 'O usuário informado não é válido.');
        }

        $dbPainel = Database::PainelDB();

        $StSQL = "
      SELECT
          D.IDDominio, D.StDominio, D.IDSisDominio, D.IDTipoServico, UD.EnTipoUsuario, AES_DECRYPT(D.StSenha, '" . PWD_KEY . "') StSenha
      FROM
          UsuarioHasDominio UD
          LEFT JOIN Dominio D USING (IDDominio)
      WHERE
          UD.IDUsuario = $IDUsuario
          AND UD.EnFavorito = 'TRUE'
      LIMIT
          1";

        if (!$Query = $dbPainel->query($StSQL)) {
            return new PC_Error('USUARIOAPI_DOMINIO_FAVORITO_LISTAR', "{$dbPainel->errno}: {$dbPainel->error}");
        }

        $ArDominioFavorito = $Query->fetch_assoc();

        # Desconsidera o site favorito se pertencer a um cliente com acesso ao painel bloqueado.
        # Adicionado por Thiago Nunes em 2016-10-10. Topico #246865.
        if (Validar::isArray($ArDominioFavorito)) {
            $ArDominioFavorito['IDSisCliente'] = CL_PegaIDCliente($ArDominioFavorito['StDominio']);
            $ArBloqueio = CL_BloqueioListar(0, $ArDominioFavorito['IDSisCliente'], 0, 'BLOQUEADO', 'PAINEL');
            if (Validar::isArray($ArBloqueio)) {
                $ArDominioFavorito = array();
            }
        }

        return $ArDominioFavorito;
    }

    /**
     * Retorna um usuário do Hostnet ID
     *
     * @param Usuario $usuario
     * @return stdClass
     */
    public static function getUsuarioHostnet($usuario)
    {
        # Inicializa o client da API do Drive
        $RestClient = new RestClient(OAUTH_CLIENT, OAUTH_CLIENT_SECRET);

        # Atualiza os tokens que serão utilizado nas apis do Hostnet ID.
        $HostnetIDToken = TSessao::getValor('HostnetIDToken');
        $HostnetIDToken = is_null($HostnetIDToken)
        ? $RestClient->createToken(URL_HOSTNETID_API . 'token')
        : $RestClient->refreshToken($HostnetIDToken);

        #
        # FIM - Credenciamento para utilizar as API do Drive e do HostnetID
        #

        $HostnetIDData = new stdClass();

        # Verifica se o cliente possui um Hostnet ID
        $Response = $RestClient->get(
            URL_HOSTNETID_API . "usuario?StEmail1={$usuario->getStUsuario()}",
            $HostnetIDToken
        );



        if (is_pc_error($Response)) {
            return new PC_Error(
            'Login',
            'Ocorreu um erro ao coletar o usuário.',
            $Response
            );
        } elseif (! empty($Response->parse())) {
            $HostnetIDData = $Response->parse();
        }

        return $HostnetIDData;
    }

    /**
     * Verifica se o usuário possui a autenticação através do google authenticator configurada
     *
     * @param Usuario $usuario
     * @return boolean
     */
    public static function hasGoogleAuthenticator(Usuario $usuario)
    {
        $ArUsuarioHostnet = self::getUsuarioHostnet($usuario);

        return empty($ArUsuarioHostnet) || empty($ArUsuarioHostnet->StGoogleAuthenticator)
        ? false
        : true;
    }

    /**
     * Retorna um usuário do Hostnet ID
     *
     * @param Usuario $usuario
     * @return stdClass
     */
    public static function setUsuarioHostnet($usuario, $StSenha)
    {
        # Inicializa o client da API do Drive
        $RestClient = new RestClient(OAUTH_CLIENT, OAUTH_CLIENT_SECRET);

        # Atualiza os tokens que serão utilizado nas apis do Hostnet ID.
        $HostnetIDToken = TSessao::getValor('HostnetIDToken');
        $HostnetIDToken = is_null($HostnetIDToken)
        ? $RestClient->createToken(URL_HOSTNETID_API . 'token')
        : $RestClient->refreshToken($HostnetIDToken);

        #
        # FIM - Credenciamento para utilizar as API do Drive e do HostnetID
        #

        # Cria o ID pelo RestClient com um post pra /usuario
        $StUrl = URL_HOSTNETID_API . 'usuario';
        $Response = $RestClient->post(
            $StUrl,
            $HostnetIDToken,
            [
                'StEmail1' => $usuario->getStUsuario(),
                'StSenha' => $StSenha
            ]
        );

        if (is_pc_error($Response)) {
            return new PC_Error(
                    'Login',
                    'Ocorreu um erro ao criar o usuário.',
                    $Response
                );
        }


        return self::getUsuarioHostnet($usuario);
    }

    /**
     * Verifica se o usuário pode utilizar o Google Authenticator no login
     *
     * @param [type] $usuario
     * @return boolean
     */
    public static function isUsuarioHostnet($usuario)
    {
        # Insira aqui um email com permissão para utilizar o Google Authenticator
        $ArUsuariosGA = [

        ];

        return (
        $usuario->getIDUsuarioTipo() == 4 # Representante
        || preg_match('/hostnet.com.br|digirati.com.br/', $usuario->getStUsuario()) # Usuários da hostnet
        || in_array($usuario->getStUsuario(), $ArUsuariosGA) # Usuários permitidos
        );
    }

    public static function editarUsuarioHostnet($usuario, $ArDados)
    {
        $ArUsuarioHostnet = self::getUsuarioHostnet($usuario);

        $ArDados['IDUsuario'] = $ArUsuarioHostnet->IDUsuario;

        # Inicializa o client da API do Drive
        $RestClient = new RestClient(OAUTH_CLIENT, OAUTH_CLIENT_SECRET);

        # Atualiza os tokens que serão utilizado nas apis do Hostnet ID.
        $HostnetIDToken = TSessao::getValor('HostnetIDToken');
        $HostnetIDToken = is_null($HostnetIDToken)
        ? $RestClient->createToken(URL_HOSTNETID_API . 'token')
        : $RestClient->refreshToken($HostnetIDToken);

        #
        # FIM - Credenciamento para utilizar as API do Drive e do HostnetID
        #

        # Cria o ID pelo RestClient com um post pra /usuario
        $StUrl = URL_HOSTNETID_API . 'usuario';
        $Response = $RestClient->put(
            $StUrl,
            $HostnetIDToken,
            $ArDados
        );

        if (is_pc_error($Response)) {
            return new PC_Error(
                    'Login',
                    'Ocorreu um erro ao criar o usuário.',
                    $Response
                );
        }


        return $Response->parse();
    }
}
