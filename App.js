/*
 * Usando ES Modules em Node.js hoje!
 * https://medium.com/@oieduardorabelo/usando-es-modules-em-node-js-hoje-278ef1fd86bf
 *https://www.npmjs.com/package/esm
 */

//node -r esm App.js tec home
//node -r esm App.js adr threads 441415 6411
require = require("esm")(module/*, options*/)
module.exports = require("./main.js")