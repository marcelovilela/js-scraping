
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require('fs');

function home() {
    console.log('netcine')
    let url = 'http://netcine.us'

    JSDOM
        //.fromURL(url)
        .fromFile('./assets/nc/_index.html')
        .then(dom => {
            fs.writeFile("assets/nc/index.html", dom.window.document.querySelector('body').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            let listPost = []

            dom.window.document.querySelectorAll('div.movie').forEach((art) => {
                //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
                    let post = {}

                    post.link = art.querySelector('a').getAttribute('href')
                    post.titulo = art.querySelector('img').getAttribute('alt')
                    post.thumb = art.querySelector('img').getAttribute('src')

                    listPost.push(post)
                //}
            })

            // +BAIXADOS
            /*dom.window.document.querySelectorAll('.wpp-list:nth-of-type(1) li').forEach((art) => {
                //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
                let post = {}

                post.link = art.querySelector('a').getAttribute('href')
                post.titulo = art.querySelector('a').getAttribute('title')
                post.thumb = art.querySelector('img.wpp-thumbnail').getAttribute('src')

                listPost.push(post)
                //}
            })*/

            //
            //
            //arquivoTexto
            let content = ''
            listPost.forEach(post => {
                content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
                content += "\n"
                content += post.link
                content += "\n"
                content += post.thumb
                content += "\n"
                //content += stringDivider(post.conteudo, 180, "\n")
                //content += "\n"
                content += "\n----------\n\n"
            })

            fs.writeFile("assets/nc/nc_index.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/nc/nc_index.json", JSON.stringify(listPost), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function post() {
    console.log('netcinePost')

    let url = process.argv[ 3 ]

    JSDOM
        //.fromURL(url)
        .fromFile('./assets/nc/_post.html')
        .then(dom => {
            fs.writeFile("assets/nc/post.html", dom.window.document.querySelector('body').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            let post = {}
            post.titulo = dom.window.document.querySelector('h1').textContent
            post.link = dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
            post.thumb = dom.window.document.querySelector('.imgs.tsll img').getAttribute('src')

            let listLink = []
            let h2 = ''
            const serverList = ['www.zippyshare.com']

            //console.log([...dom.window.document.querySelector('.entry').children])

            console.log(post)

            // <li class="tab-pane " id="ert_pane1-4"><strong>Opções de Downloads:</strong>
            dom.window.document.querySelectorAll('li.has-sub ul li').forEach((el) => {
                let l = {}

                const magnet = el.querySelector('a').getAttribute('href')
                let text = `[[${el.closest('.has-sub').querySelector('a').textContent} ]] `
                text += el.querySelector('.datex').textContent
                l = { url: magnet, text, type: 'protected-stream' }
                listLink.push(l)
            })

            post.listLink = listLink

            //
            //
            //arquivoTexto
            let content = ''
            content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += "-> " + post.link
            content += "\n"
            content += "#######################################################"
            content += "\n\n"
            post.listLink.forEach(post => {
                content += "\n"
                content += post.text
                content += "\n"
                content += post.url
                content += "\n\n"
            })

            fs.writeFile("assets/nc/nc_post.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/nc/nc_post.json", JSON.stringify(post), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function postSelect() {
    console.log('netcinePostSelect')

    let url = process.argv[ 3 ]

    JSDOM
        //.fromURL('http://netcine.us/episode/tgd-2x01/')
        .fromFile('./assets/nc/post_select.html')
        .then(dom => {
            /*fs.writeFile("assets/nc/post_select.html", dom.window.document.querySelector('body').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })*/

            let post = {}
            post.titulo = dom.window.document.querySelector('.datos.episodio p').textContent
            post.link = dom.window.document.querySelector('a.tw').getAttribute('data-rurl')
            post.thumb = '' // dom.window.document.querySelector('.imgs.tsll img').getAttribute('src')

            let listLink = []
            let h2 = ''
            const serverList = [ 'www.zippyshare.com' ]

            //console.log([...dom.window.document.querySelector('.entry').children])

            console.log(post)

            // <li class="tab-pane " id="ert_pane1-4"><strong>Opções de Downloads:</strong>
            dom.window.document.querySelectorAll('#player-container .player-content').forEach((el) => {
                let l = {}

                const magnet = el.querySelector('iframe').getAttribute('src')
                let text = ''
                l = { url: magnet, text, type: 'protected-stream' }
                listLink.push(l)
            })

            post.listLink = listLink

            //
            //
            //arquivoTexto
            let content = ''
            content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += "-> " + post.link
            content += "\n"
            content += "#######################################################"
            content += "\n\n"
            post.listLink.forEach(post => {
                content += "\n"
                content += post.text
                content += "\n"
                content += post.url
                content += "\n\n"
            })

            fs.writeFile("assets/nc/nc_post_select.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/nc/nc_post_select.json", JSON.stringify(post), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function stringDivider(str, width, spaceReplacer) {
    if (str.length > width) {
        var p = width
        for (; p > 0 && str[ p ] != ' '; p--) {
        }
        if (p > 0) {
            var left = str.substring(0, p);
            var right = str.substring(p + 1);
            return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
        }
    }
    return str;
}

switch (process.argv[ 2 ]) {
    case 'post': postSelect();// node ol.js post https://olhardigital.com.br/ces-2019/
        break;
    default: home();
}
