import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Index extends Scrap {
    constructor(){
        super()
        this.utils = new Utils()
    }

    /**
    * Analisa conteudo da pagina: home (listagem de secoes)
    */
    async home(page) {
        const url = 'https://adrenaline.uol.com.br/forum/'

        // Pega html da pagina, entao converte em objeto
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)
        
        // Analisa o conteudo da pagina
        let listSection = []

        dom.window.document.querySelectorAll('li.node.level_1').forEach((button) => {
            let section = {}
            section.titulo = button.querySelector('h3.nodeTitle').textContent
            section.listForum = []

            button.querySelectorAll('li.node.level_2').forEach((el) => {
                let forum = {}
                forum.link = el.querySelector('h3 a').getAttribute('href')
                forum.titulo = el.querySelector('h3').textContent
                forum.discussoes = el.querySelector('.nodeStats dl:nth-of-type(1)').textContent
                forum.mensagens = el.querySelector('.nodeStats dl:nth-of-type(2)').textContent
                section.listForum.push(forum)
            })

            listSection.push(section)
        })
        
        return listSection
    }

    /**
     * Armazena conteudo da respectiva pagina analisada em arquivo
     */
    homeDump(listSection, pathPrefix) {
        let content = ''
        listSection.forEach(section => {
            content += '\n#\n#\n#\n'
            content += section.titulo
            content += '\n'
            section.listForum.forEach(forum => {
                content += forum.titulo
                content += '\n'
                content += forum.link
                content += '\n\n'
            })
        })

        this.utils.dump(`${pathPrefix}assets\\adr\\adr_index.txt`, content)
        this.utils.dump(`${pathPrefix}assets\\adr\\adr_index.json`, JSON.stringify(listSection))

        myJsonAPI.update('12c7n2', listSection)
        .then((updatedJSON) => console.log(updatedJSON))
    }

    /**
    * Analisa conteudo da pagina: secao (listagem de topicos)
    */
    async forums(id) {
        // Gera a url, exemplo
        let url = 'https://adrenaline.uol.com.br/forum/forums/retro.224/'

        if (!isNaN(id) && id > 0) {
            url = `https://adrenaline.uol.com.br/forum/forums/${id}/`
        }

        // Pega html da pagina, entao converte em objeto
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        // Analisa o conteudo da pagina
        let listPost = []

        dom.window.document.querySelectorAll('.discussionListItem').forEach((button) => {
            let post = {}
            post.link = button.querySelector('h3 a').getAttribute('href')
            post.titulo = button.querySelector('h3').textContent
            post.autor = `${button.querySelector('.posterDate .username').textContent} ${button.querySelector('.posterDate .faint').textContent}`
            post.respostas = button.querySelector('.listBlock .major').textContent
            post.visitas = button.querySelector('.listBlock .minor').textContent
            post.ultimaMensagem = button.querySelector('.lastPostInfo .username') != null && button.querySelector('.lastPostInfo .dateTime') != null
                ? `${button.querySelector('.lastPostInfo .username').textContent} ${button.querySelector('.lastPostInfo .dateTime').textContent}`
                : ''
            post.paginas = button.querySelector('.itemPageNav') !== null
                ? button.querySelector('.itemPageNav a:last-of-type').textContent
                : 1
            listPost.push(post)
        });

        return listPost
    }

    /**
     * Armazena conteudo da respectiva pagina analisada em arquivo
     */
    forumsDump(listPost, pathPrefix) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, '').replace(/\n/g, '')
            content += '\n'
            content += `${post.link} , ${post.paginas} páginas`
            content += '\n'
            content += post.ultimaMensagem
            content += '\n'
            content += '\n----------\n\n'
        })

        this.utils.dump(`${pathPrefix}assets\\adr\\adr_forums.txt`, content)
        this.utils.dump(`${pathPrefix}assets\\adr\\adr_forums.json`, JSON.stringify(listPost))

        myJsonAPI.update('12xn8u', listPost)
        .then((updatedJSON) => console.log(updatedJSON))
    }

    /**
    * Analisa conteudo da pagina: topico
    */
    async threads(id, page) {
        // Gera a url, exemplo: https://adrenaline.uol.com.br/forum/threads/xbox-one-topico-oficial.573331/
        let url = 'https://adrenaline.uol.com.br/forum/threads/573331/'
        if (!isNaN(id)) {
            const pageParam = page > 0 ? `/page-${page}` : ''
            url = `https://adrenaline.uol.com.br/forum/threads/${id}${pageParam}`
        }

        // Pega html da pagina, entao converte em objeto
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        // Analisa o conteudo da pagina
        let post = {}

        post.titulo = dom.window.document.querySelector('h1').textContent

        post.pagAtual = 1
        post.pagUltima = 1
        post.pagAtual = dom.window.document.querySelector('.PageNav') !== null
            ? dom.window.document.querySelector('.PageNav').getAttribute('data-page')
            : post.pagAtual
        post.pagUltima = dom.window.document.querySelector('.PageNav') !== null
            ? dom.window.document.querySelector('.PageNav').getAttribute('data-last')
            : post.pagUltima

        let listMessage = []

        dom.window.document.querySelectorAll('.messageList .message').forEach((button) => {
            let message = {}
            message.conteudoTextual = button.querySelector('.messageContent .messageText').textContent
            message.conteudoMensagem = [{msg: button.querySelector('.messageContent .messageText').textContent}]
            message.assinatura = button.querySelector('.signature') !== null
                ? button.querySelector('.signature').textContent
                : ''

            message.autor = button.querySelector('.privateControls .username').textContent
            message.data = button.querySelector('.privateControls .DateTime').textContent

            //
            message.usuario = {}
            message.usuario.avatar = button.querySelector('.messageUserInfo img').getAttribute('src')
            message.usuario.link = button.querySelector('.messageUserInfo .username').getAttribute('href')
            message.usuario.nome = button.querySelector('.messageUserInfo .username').textContent
            message.usuario.titulo = button.querySelector('.messageUserInfo .userTitle').textContent
            message.usuario.banner = button.querySelector('.messageUserInfo .userBanner').textContent
            //
            message.usuario.registro = button.querySelector('.extraUserInfo dl:nth-of-type(1)').textContent
            message.usuario.mensagens = button.querySelector('.extraUserInfo dl:nth-of-type(2)').textContent
            message.usuario.curtidas = button.querySelector('.extraUserInfo dl:nth-of-type(3)').textContent
            message.usuario.trofeus = button.querySelector('.extraUserInfo dl:nth-of-type(4)').textContent
            message.usuario.localizacao = button.querySelector('.extraUserInfo dl:nth-of-type(5)') !== null
                ? button.querySelector('.extraUserInfo dl:nth-of-type(5)').textContent
                : '---'

            listMessage.push(message)
        });

        post.listMessage = listMessage
        return post
    }

    /**
    * Armazena conteudo da respectiva pagina analisada em arquivo
    */
    threadsDump(post, pathPrefix){
        let content = ''
        post.listMessage.forEach(msg => {
            //content += '\n#\n#\n#\n'
            content += '\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
            content += '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n'
            content += `=>${msg.usuario.nome} - ${msg.data}\n`
            //content += msg.conteudoTextual
            //textwrap js
            content += this.utils.stringDivider(
                msg.conteudoTextual.replace(/\t/g, ''),
                180,
                '\n'
            )
            content += '\n\n'
        })
        content += `Página ${post.pagAtual} de ${post.pagUltima}`

        this.utils.dump(`${pathPrefix}assets\\adr\\adr_post.txt`, content)
        this.utils.dump(`${pathPrefix}assets\\adr\\adr_post.json`, JSON.stringify(post))
    
        myJsonAPI.update('9relq', post)
        .then((updatedJSON) => console.log(updatedJSON))
    }
}

//module.exports = Index