import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Psc extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': '1gdrct',//psi
            'binForEachPost': ''
        }
    }
    async home() {
        console.log('ps4club')
        let url = 'http://turbofilmesonline.club/ps4brasil/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/psc/_index.html", dom.window.document.querySelector('body').outerHTML)

        let home = {'listPost': []}

        dom.window.document.querySelectorAll('.lista-filmes li').forEach((art) => {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')

            if (art.querySelector('.capa img') !== null && !art.querySelector('.capa img').getAttribute('src').includes('base64')) {
                post.thumb = art.querySelector('.capa img').getAttribute('src')
            }

            home.listPost.push(post)
        })

        return home
    }

    homeDump(listPost, path) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.thumb
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/psc/psc_index.txt`, content)
        this.utils.dump(`${path}assets/psc/psc_index.json`, JSON.stringify(listPost))
    }

    async post(url) {
        console.log('ps4clubPost')

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/psc/_post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h1').textContent
        post.link = dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
        post.thumb = dom.window.document.querySelector('.capa-single') !== null
            ? dom.window.document.querySelector('.capa-single img').getAttribute('src')
            : ''
        //console.log(post)
        let listLink = []
        const serverList = [ 'www.zippyshare.com' ]

        //console.log([...dom.window.document.querySelector('.entry').children])
        //return false
        //content-single

        dom.window.document.querySelectorAll('.content-single a').forEach((link) => {
            let l = {}

            if (link.getAttribute('href').includes('magnet')) {
                l = { url: link.getAttribute('href'), text: link.textContent, type: 'magnet' }
                listLink.push(l)
            }

            const site = link.getAttribute('href').split('/')[ 2 ]

            if (serverList.indexOf(site) > -1) {
                console.log('nao eh torrent')
                l = { url: link.getAttribute('href'), text: link.textContent, type: 'server' }
                listLink.push(l)
            }
        })

        post.listLink = listLink
        return post
    }

    postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/psc/psc_post.txt`, content)
        this.utils.dump(`${path}assets/psc/psc_post.json`, JSON.stringify(post))
    }
}

//module.exports = Psc
