import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

import AutumnFallsHD from '../assets/planet/AutumnFalls/data_AutumnFallsHD.json'
import PiperPerriHD from '../assets/planet/PiperPerri/data_PiperPerriHD.json'
import KarleeGreyHD from '../assets/planet/KarleeGrey/data_KarleeGreyHD.json'
import AnastasiaKnightHD from '../assets/planet/AnastasiaKnight/data_AnastasiaKnightHD.json'
import GinaValentinaHD from '../assets/planet/GinaValentina/data_GinaValentinaHD.json'
import NicoletteSheaHD from '../assets/planet/NicoletteShea/data_NicoletteSheaHD.json'
import KissaSinsHD from '../assets/planet/KissaSins/data_KissaSinsHD.json'

//
import ChloeAmourHD from '../assets/planet/ChloeAmour/data_ChloeAmourHD.json'
import ChloeAmourPHOTOS from '../assets/planet/ChloeAmour/data_ChloeAmourPHOTOS.json'
import ChloeAmourSD from '../assets/planet/ChloeAmour/data_ChloeAmourSD.json'

import JennaHazeHD from '../assets/planet/JennaHaze/data_JennaHazeHD.json'
import JennaHazePHOTOS from '../assets/planet/JennaHaze/data_JennaHazePHOTOS.json'
import JennaHazeSD from '../assets/planet/JennaHaze/data_JennaHazeSD.json'

import ValentinaNappiHD from '../assets/planet/ValentinaNappi/data_ValentinaNappiHD.json'
import ValentinaNappiPHOTOS from '../assets/planet/ValentinaNappi/data_ValentinaNappiPHOTOS.json'
import ValentinaNappiSD from '../assets/planet/ValentinaNappi/data_ValentinaNappiSD.json'

export default class Planet extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
    }
    async home() {
        console.log('ps4iso')
        let url = 'http://ps4iso.net/most-popular-ps4-games'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("../assets/psi/index.html", dom.window.document.querySelector('body').outerHTML)

        let listPost = []

        dom.window.document.querySelectorAll('.entry-content li').forEach((art) => {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')

            post.thumb = ''
            /*if (art.querySelector('.capa img') !== null && !art.querySelector('.capa img').getAttribute('src').includes('base64')) {
                post.thumb = art.querySelector('.capa img').getAttribute('src')
            }*/

            listPost.push(post)
        })

        return listPost
    }

    homeDump(listPost, path) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            //content += post.thumb
            //content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}../assets/psi/psi_index.txt`, content)
        this.utils.dump(`${path}../assets/psi/psi_index.txt`, JSON.stringify(listPost))
    }

    postParse(dom, page){
        let post = {}
        let listPost = []

        dom.window.document.querySelectorAll('table[id]').forEach((art) => {
            let post = {}

            const postMessage = art.querySelector('div[id^="post_message"]').textContent
            //post.content = postMessage
            post.titulo = postMessage.split('\n')[0] !== ''
                ? postMessage.split('\n')[0]
                : postMessage.split('\n').filter(val => val !== '')[0]

            post.id = art.querySelector('a[id^="postcount"]').textContent.padStart(4, '0')
            post.link = art.querySelector('a[id^="postcount"]').getAttribute('href')

            post.data = art.querySelector('td.thead:nth-of-type(1)')
            .textContent.replace(/\n/g, '').replace(/\t/g, '')

            post.listImage = []
            art.querySelectorAll('div[id^="post_message"] img').forEach((img) => {
                post.listImage.push(img.getAttribute('src'))
            })

            post.listLink = []
            art.querySelectorAll('div[id^="post_message"] a').forEach((a) => {
                //desconsidera link para imagem, o que importa aqui eh o link para download
                if (a.querySelector('img') === null) {
                    post.listLink.push(a.getAttribute('href'))
                }
            })
            postMessage.split('\n').filter(val => val.includes('http')).forEach((str) => {
                post.listLink.push(str)
            })

            listPost.push(post)
        })

        post.listPost = listPost
        post.titulo = dom.window.document.querySelector('meta[name="description"]').getAttribute('content')

        post.pagAtual = dom.window.document.querySelector('.pagenav .alt2').textContent
        post.pagUltima = dom.window.document.querySelector('.pagenav .alt1:last-of-type a') !== null
        ? dom.window.document.querySelector('.pagenav .alt1:last-of-type a').getAttribute('href').split('=').pop()
        : page

        return post
    }

    async post(uri, page = 1, sufix = 'StarName', category = 'HD') {
        let html = ''
        if(uri.includes('http')){
            console.log('planetPost-requisicao')
            html = await this.requestUrl(uri)
            if(page !== 999) {
                this.utils.dump(`./assets/planet/${sufix}/post_${sufix}${category}_${page}.html`, html)
            }
        } else {
            console.log('planetPost-leituraDeArquivo')
            html = await this.utils.getContent(uri)
        }
        
        const dom = await this.HTMLparse(html)

        const post = this.postParse(dom, page)

        return post
    }

    postDump(post, path) {
        //this.utils.dump(`${path}../assets/planet/post.json`, JSON.stringify(post))
    }

    // Mescla as pagina armazenadas localmente como html em um arquivo json
    // node -r esm App.js planet postMerge ValentinaNappi HD
    async postMerge(star = 'StarName', category = 'HD') {
        const _uri = `./assets/planet/${star}/post_${star}${category}_999.html`

        let url = ''
        url = 'http://www.planetsuzy.org/t609597-p999-valentina-nappi.html'
        url = 'http://www.planetsuzy.org/t765371-p999-piper-perri.html'
        url = 'http://www.planetsuzy.org/t803895-p999-gina-valentina.html'
        url = 'http://www.planetsuzy.org/t901495-p999-anastasia-knight.html'
        url = 'http://www.planetsuzy.org/t754742-p999-karlee-grey.html'
        url = 'http://www.planetsuzy.org/t938811-p999-autumn-falls.html'
        url = 'http://www.planetsuzy.org/t878195-p999-nicolette-shea.html'
        url = 'http://www.planetsuzy.org/t772557-p999-kissa-sins.html'

        /**
         * Descobre a ultima pagina 
         */
        //const post = await this.post(url.replace('999', 1), 1, 'ValentinaNappi', 'HD')
        const post = await this.post(url.replace('999', 1), 999, star, category)

        /*
         * Baixa as paginas faltando 
         */
        let transferiu = false
        for(let page = 1; page <= post.pagUltima; page++) {
            const html = await this.utils.getContent(_uri.replace('999', page))
            if (!html) {
                //console.log(`Transferindo... Pagina atual: ${page}`)
                await this.post(url.replace('999', page), page, star, category)
                if (transferiu === false && page -1 > 0) {
                    transferiu = true
                    await this.post(url.replace('999', page - 1), page - 1, star, category)
                }
            }
        }
        
        /**
         * Merge em jsom 
         */
        url = _uri
        let merge = []
        for(let page = 1; page <= post.pagUltima; page++) {
            const currentPost = await this.post(url.replace('999', page), page, star, category)
            console.log(`Mesclando... Pagina atual: ${page}`)
            //merge.push(currentPost.listPost)
            merge = [...merge, ...currentPost.listPost]
        }

        this.postMergeDump({listPost: merge}, './', star, category)

        return {listPost: merge}
    }

    // Requisita desde a pagina indicada ate a ultima
    async postRefresh(url, pageStart = 1, star = 'StarName', category = 'HD') {
        //vn-sd
        //const url = 'http://www.planetsuzy.org/t590138-p999-valentina-nappi-valentina-valentina-b.html'
        
        //const post = await this.post(url.replace('999', 1), 1, 'ValentinaNappi', 'HD')
        const post = await this.post(url.replace('999', pageStart), pageStart, star, category)
        
        let merge = []
        for(let page = pageStart; page <= post.pagUltima; page++) {
            const currentPost = await this.post(url.replace('999', page), page, star, category)
            console.log(`Atualizando... Pagina atual ${page}`)
            merge.push(currentPost.listPost)
        }

        await this.postMerge(star, category)

        return merge
    }

    postMergeDump(merge, path, star = '', category = '') {
        if(star && category){
            console.log(`./assets/planet/${star}/data_${star}${category}.json`)
            this.utils.dump(`./assets/planet/${star}/data_${star}${category}.json`, JSON.stringify(merge))
        }        
    }

}

//module.exports = Psi

            //vn-hd
            //http://www.planetsuzy.org/t609597-p${page}-valentina-nappi.html
            //vn-sd
            //http://www.planetsuzy.org/t590138-p999-valentina-nappi-valentina-valentina-b.html
            //vn-photos
            //http://www.planetsuzy.org/t631886-p999-valentina-nappi-valentina-valentina-b.html
            //ca-hd
            //http://www.planetsuzy.org/t669400-p999-chloe-amour.html
            //ca-sd
            //http://www.planetsuzy.org/t667852-p999-chloe-amour.html
            //ca-photos
            //http://www.planetsuzy.org/t681058-p999-chloe-amour.html
            //jh-hd
            //http://www.planetsuzy.org/t359479-p999-jenna-haze.html
            //jh-sd
            //http://www.planetsuzy.org/t183446-p999-jenna-haze.html
            //jh-photos
            //http://www.planetsuzy.org/t359480-p${page}-jenna-haze.html
            /*for(let page = 1; page <= 30; page++) {
                console.log('args', process.argv[ 3 ])
                await i[ process.argv[ 3 ] ](
                `http://www.planetsuzy.org/t359480-p${page}-jenna-haze.html`, 
                page, 
                'JennaHazePHOTOS'
                )
            }*/

//import homePl from './assets/planet/ChloeAmour/data_ChloeAmourHD.json'

const listStars = [
    "Autumn Falls", 
    "Piper Perri", 
    "Karlee Grey", 
    "Anastasia Knight", 
    "Gina Valentina", 
    "Nicolette Shea", 
    "Kissa Sins",

    "Chloe Amour", 
    "Valentina Nappi",
    "Jenna Haze" 
]
const o = {}
listStars.forEach(name => {
	const namePath = name.replace(' ', '')
	let categoryCode = {}
	categoryCode[name+' HD'] = namePath+'HD'
	categoryCode[name+' PHOTOS'] = namePath+'PHOTOS'
	categoryCode[name+' SD'] = namePath+'SD'
	o[namePath] = {name, categoryCode}
})

//const i = {}
//listStars.forEach(name => {
//	const namePath = name.replace(' ', '')
//	i[namePath+'HD'] = namePath+'HD'
//})

export const postPl = {
    AutumnFallsHD,
    PiperPerriHD,
    KarleeGreyHD,
    AnastasiaKnightHD,
    GinaValentinaHD,
    NicoletteSheaHD,
    KissaSinsHD,
    
    ChloeAmourHD,
    ChloeAmourPHOTOS,
    ChloeAmourSD,

    JennaHazeHD,
    JennaHazePHOTOS,
    JennaHazeSD,

    ValentinaNappiHD,
    ValentinaNappiPHOTOS,
    ValentinaNappiSD,

    //model: {...o},
    _model: {
        'ChloeAmour': {
            name: 'Chloe Amour', 
            categoryCode: {
                'Chloe Amour HD': 'ChloeAmourHD',
                'Chloe Amour PHOTOS': 'ChloeAmourPHOTOS',
                'Chloe Amour SD': 'ChloeAmourSD'
            }
        },
        'JennaHaze': {
            name: 'Jenna Haze',
            categoryCode: {
                'Jenna Haze HD': 'JennaHazeHD',
                'Jenna Haze PHOTOS': 'JennaHazePHOTOS',
                'Jenna Haze SD': 'JennaHazeSD'
            }
        },
        'ValentinaNappi': {
            name: 'Valentina Nappi', 
            categoryCode: {
                'Valentina Nappi HD': 'ValentinaNappiHD',
                'Valentina Nappi PHOTOS': 'ValentinaNappiPHOTOS',
                'Valentina Nappi SD': 'ValentinaNappiSD'
            }
        }
    }
}

export const _postPl = {
    models: {
        'ChloeAmour': 'Chloe Amour', 
        'JennaHaze': 'Jenna Haze', 
        'ValentinaNappi': 'Valentina Nappi'
    },
    listPost: {
        'Chloe Amour': {
            ChloeAmourHD,
            ChloeAmourPHOTOS,
            ChloeAmourSD
        },
        'Jenna Haze': {
            JennaHazeHD,
            JennaHazePHOTOS,
            JennaHazeSD
        },
        'Valentina Nappi': {
            ValentinaNappiHD,
            ValentinaNappiPHOTOS,
            ValentinaNappiSD
        }
    }
}
