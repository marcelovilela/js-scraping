import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Gn extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
    }

    async home() {
        const url = 'https://br.ign.com/forum/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let listSection = []

        dom.window.document.querySelectorAll('.block-container').forEach((button) => {
            if (button.querySelector('.block-header') !== null) {
                let section = {}
                section.titulo = button.querySelector('.block-header').textContent.replace(/\t/g, "").replace(/\n/g, "")
                section.listForum = []

                button.querySelectorAll('.block-body .node').forEach((el) => {
                    let forum = {}
                    forum.link = el.querySelector('.node-title a').getAttribute('href').replace(/\t/g, "").replace(/\n/g, "")
                    forum.titulo = el.querySelector('.node-title').textContent.replace(/\t/g, "").replace(/\n/g, "")
                    forum.discussoes = el.querySelector('.node-stats dl:nth-of-type(1)').textContent.replace(/\t/g, "").replace(/\n/g, "")
                    forum.mensagens = el.querySelector('.node-stats dl:nth-of-type(2)').textContent.replace(/\t/g, "").replace(/\n/g, "")
                    section.listForum.push(forum)
                })

                listSection.push(section)
            }
        })
        
        return listSection
    }

    homeDump(listSection, path) {
        let content = ''
        listSection.forEach(section => {
            content += "\n#\n#\n#\n"
            content += section.titulo
            content += "\n"
            section.listForum.forEach(forum => {
                content += forum.titulo
                content += "\n"
                content += forum.link
                content += "\n\n"
            })
        })

        this.utils.dump(`${path}assets/gn/gn_index.txt`, content)
        this.utils.dump(`${path}assets/gn/gn_index.json`, JSON.stringify(listSection))
    }

    async forums(id) {
        let url = 'https://br.ign.com/forum/forums/24/'

        if (!isNaN(id) && id > 0) {
            url = `https://br.ign.com/forum/forums/${id}/`
        }

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let listPost = []

        dom.window.document.querySelectorAll('.structItemContainer .structItem').forEach((button) => {
            let post = {}
            post.link = button.querySelector('.structItem-title a').getAttribute('href')
            post.titulo = button.querySelector('.structItem-title a').textContent
            post.autor = button.querySelector('.structItem-minor .structItem-parts:nth-of-type(1)').textContent.replace(/\n/g, "")
            post.respostas = button.querySelector('.pairs.pairs--justified:nth-of-type(1)').textContent
            post.visitas = button.querySelector('.pairs.pairs--justified:nth-of-type(2)').textContent
            //.structItem-cell.structItem-cell--latest .structItem-minor
            const ultAutor = button.querySelector('.structItem-cell.structItem-cell--latest .structItem-minor').textContent
            const ultData = button.querySelector('.structItem-cell.structItem-cell--latest a').textContent
            post.ultimaMensagem = `${ultAutor} ${ultData}`
            post.ultimaMensagem = post.ultimaMensagem.replace(/\n/g, "")
            post.paginas = button.querySelector('.structItem-pageJump') !== null
                ? button.querySelector('.structItem-pageJump a:last-of-type').textContent
                : 1

            listPost.push(post)
        });

        return listPost
    }

    forumsDump(listPost, path) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += `${post.link} , ${post.paginas} páginas`
            content += "\n"
            content += post.ultimaMensagem
            content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/gn/gn_forums.txt`, content)
        this.utils.dump(`${path}assets/gn/gn_forums.json`, JSON.stringify(listPost))
    }

    async threads(id, paginaNumero) {
        //https://br.ign.com/forum/threads/fixo-t%C3%B3pico-de-apresenta%C3%A7%C3%B5es-e-boas-vindas.640/
        let url = 'https://br.ign.com/forum/threads/640/'

        if (!isNaN(id) && id > 0) {
            let pagina = paginaNumero > 0 ? `/page-${paginaNumero}` : ''
            url = `https://br.ign.com/forum/threads/${id}${pagina}`
        }

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let post = {}

        post.titulo = dom.window.document.querySelector('h1').textContent

        post.pagAtual = 1
        post.pagUltima = 1
        /*post.pagAtual = dom.window.document.querySelector('.PageNav') !== null 
            ? dom.window.document.querySelector('.PageNav').getAttribute('data-page')
            : post.pagAtual
        post.pagUltima = dom.window.document.querySelector('.PageNav') !== null 
            ? dom.window.document.querySelector('.PageNav').getAttribute('data-last')
            : post.pagUltima*/

        let listMessage = []

        dom.window.document.querySelectorAll('.block-body .message').forEach((button) => {
            let message = {}

            message.conteudo = button.querySelector('.message-content .message-body').textContent

            let children = button.querySelector('.message-content .message-body').children
            children = [ ...children ]

            message.assinatura = button.querySelector('.signature') !== null
                ? button.querySelector('.signature').textContent
                : ''

            message.autor = ''//button.querySelector('.privateControls .username').textContent
            message.data = button.querySelector('.message-attribution a').textContent

            //
            message.usuario = {}
            message.usuario.avatar = button.querySelector('.message-cell--user img').getAttribute('src')
            message.usuario.link = button.querySelector('.message-cell--user .message-avatar-wrapper a').getAttribute('href')
            message.usuario.nome = button.querySelector('.message-cell--user .message-name').textContent
            message.usuario.titulo = button.querySelector('.message-cell--user .message-name').textContent
            message.usuario.banner = button.querySelector('.message-cell--user .userTitle.message-userTitle').textContent
            //
            /*message.usuario.registro = button.querySelector('.extraUserInfo dl:nth-of-type(1)').textContent
            message.usuario.mensagens = button.querySelector('.extraUserInfo dl:nth-of-type(2)').textContent
            message.usuario.curtidas = button.querySelector('.extraUserInfo dl:nth-of-type(3)').textContent
            message.usuario.trofeus = button.querySelector('.extraUserInfo dl:nth-of-type(4)').textContent
            message.usuario.localizacao = button.querySelector('.extraUserInfo dl:nth-of-type(5)') !== null
                ? button.querySelector('.extraUserInfo dl:nth-of-type(5)').textContent
                : '---'*/

            listMessage.push(message)
        });

        post.listMessage = listMessage
        return post
    }

    threadsDump(post, path){
        let content = ''
        post.listMessage.forEach(msg => {
            content += "\n#\n#\n#\n"
            content += msg.usuario.nome
            content += "\n"
            content += '//'
            content += "\n"
            //content += msg.conteudo
            //textwrap js
            //content += msg.conteudo.replace(/\t/g, "")
            content += this.utils.stringDivider(
                msg.conteudo.replace(/\t/g, ""),
                180,
                "\n"
            )
            content += "\n"
            content += msg.data
            content += "\n"
        })
        content += `Página ${post.pagAtual} de ${post.pagUltima}`

        this.utils.dump(`${path}assets/gn/gn_post.txt`, content)
        this.utils.dump(`${path}assets/gn/gn_post.json`, JSON.stringify(post))
    }
}

//module.exports = Gn
