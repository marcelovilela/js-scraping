import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Wcc extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'dzznq',
            'binForEachPost': 'wgfeu'
        }
    }

    async home() {
        console.log('wccftech')
        let url = 'https://www.wccftech.com/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let home = {}
        /*home.pgAtual = dom.window.document.querySelector('#page-selection-area li.active').textContent
        //home.pgUltima = dom.window.document.querySelector('#page-selection-area li:nth-of-type(6)').textContent
        home.pgUltima = 1
        dom.window.document.querySelectorAll('#page-selection-area li').forEach((pag) => {
            home.pgUltima = home.pgUltima < pag.textContent && !isNaN(parseInt(pag.textContent)) ? pag.textContent : home.pgUltima
            //console.log(`${pag.textContent} > ${home.pgUltima} ... ${home.pgUltima}`)
        })*/
        let listPost = []

        //<article id="post-275062" class="bloco">
        dom.window.document.querySelectorAll('.content .post').forEach((art) => {
            let post = {}
            //console.log(art.outerHTML)
            //return false
            if (art.querySelector('h2 a') !== null) {
                post.link = art.querySelector('h2 a').getAttribute('href')
                console.log(post.link)
                post.titulo = art.querySelector('h2').textContent
                console.log(post.titulo)
                post.thumb = art.querySelector('div.thumb img').getAttribute('src')
                console.log(post.thumb)
                //post.preview = art.querySelector('p').textContent
                //post.conteudo = art.querySelector('.entry p').textContent
                post.conteudo = []

                //post.img = art.querySelector('.thumb img').getAttribute('data-lazy-src')

                listPost.push(post)
            }
        })

        home.listPost = listPost
        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            //content += "\n"
            //content += post.preview.slice(0, 100)
            //content += "\n"
            content += "\n\n----------\n\n"
        })
        content += `Página ${home.pgAtual} de ${home.pgUltima}`

        this.utils.dump(`${path}assets/wcc/wcc_index.txt`, content)
        this.utils.dump(`${path}assets/wcc/wcc_index.json`, JSON.stringify(home))
        myJsonAPI.update(this.myJsonIds.home, home)
        .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        console.log('wcc-post')
        // let url = 'https://wccftech.com/resident-evil-tv-series-work-netflix/'

        const html = await this.requestUrl(url)
        // const html = await this.utils.getContent('./assets/wcc/_wcc_post.html')
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/wcc/_wcc_post.html", dom.window.document.querySelector('body').outerHTML)

        //<article id="post-275062" class="bloco">
        const art = dom.window.document.querySelector('section.content')
        let post = {}
        post.link = dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
        post.titulo = art.querySelector('h1').textContent
        //post.thumb = art.querySelector('.featured-image img').getAttribute('src')

        //post.conteudo = art.querySelector('.entry p').textContent
        post.listContent = []

        let children = art.querySelector('.body.clearfix').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}
            //console.log(el.tagName)

            if (el.getAttribute('class') !== null && el.getAttribute('class') === 'featured-image') {
                post.thumb = el.querySelector('img').getAttribute('src')
            }
            else if (el.querySelector('.wccf_related_post') !== null) {
                //console.log('wccf_related_post')
            }
            else if (el.tagName === 'DIV') {
                //console.log('DIV')
            }
            else if (el.querySelector('img') !== null) {
                //console.log('el img')
                //console.log(el)
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('src')
            }
            else if (el.querySelector('.video-container') !== null) {
                //console.log('el img')
                //console.log(el)
                c.tipo = 'video'
                c.conteudo = el.querySelector('.video-container iframe').getAttribute('data-src')
            }
            else if (el.tagName === 'P' && el.getAttribute('class') === null && el.getAttribute('id') === null) {
                c.tipo = 'texto'
                c.conteudo = el.textContent
                //console.log('p.content')
                //console.log(c.conteudo)
            }
            else if (el.tagName === 'BLOCKQUOTE') {
                c.tipo = 'texto-em-comentario'
                c.conteudo = el.textContent
                //console.log('p.content')
                //console.log(c.conteudo)
            }

            if ('conteudo' in c && c.conteudo) {
                post.listContent.push(c)
            }
        })
        return post
    }

    async postDump(post, path) {
        let content = ''

        content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += post.link
        content += "\n"
        //content += post.conteudo[ 0 ].conteudo.slice(0, 100)

        post.listContent.forEach(cont => {
            content += "\n"
            content += cont.tipo === 'texto' ? this.utils.stringDivider(cont.conteudo, 180, "\n") : cont.conteudo
            content += "\n"
        })

        content += "\n"
        content += "\n----------\n\n"

        this.utils.dump(`${path}assets/wcc/wcc_post.txt`, content)
        this.utils.dump(`${path}assets/wcc/wcc_post.json`, JSON.stringify(post))


        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = this.myJsonIds.binForEachPost

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Wcc
