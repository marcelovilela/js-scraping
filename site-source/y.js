const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require('fs');

function home() {
    console.log('yahoo')
    let url = 'https://br.yahoo.com/?p=us'

    JSDOM
        .fromURL(url)
        //.fromFile('./assets/y/_y_index@2019.01.30.html')
        .then(dom => {
            fs.writeFile("assets/y/_y_index.html", dom.window.document.querySelector('body').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            let listPost = []
            let cat = {}

            dom.window.document.querySelectorAll('.js-stream-item-wrap').forEach((art) => {
                let post = {}
                if (art.querySelector('h3 a') !== null) {
                    post.titulo = art.querySelector('h3').textContent
                    
                    post.link = art.querySelector('h3 a') !== null
                        ? art.querySelector('h3 a').getAttribute('href')
                        : ''
                    //post.data = art.querySelector('[itemprop="dateModified"]').getAttribute('content')
                    //post.thumb = art.querySelector('[itemprop="image"]').getAttribute('content')

                    post.categoria = { titulo: '', direcionamento: '' }
                    //if (art.querySelector('.js-content-label') !== null) {
                        post.categoria = {
                            titulo: art.querySelector('h3 a').getAttribute('href').split('/')[2],
                            direcionamento: art.querySelector('h3 a').getAttribute('href')
                        }
                        cat[post.categoria.titulo] = post.categoria.direcionamento
                    //}
                    
                    listPost.push(post)
                }
            })

            //console.log(listPost)

            //
            //
            //arquivoTexto
            let content = ''
            listPost.forEach(post => {
                content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
                content += "\n"
                content += post.link
                content += "\n"
                //content += stringDivider(post.conteudo, 180, "\n")
                //content += "\n"
                content += "\n----------\n\n"
            })

            fs.writeFile("assets/y/y_index.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/y/y_index.json", JSON.stringify(listPost), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function post() {
    console.log('yahooPost')

    let url = process.argv[ 3 ]

    JSDOM
        .fromURL(url)
        //.fromFile('./assets/ad/_ad_post.html')
        .then(dom => {
            fs.writeFile("assets/ad/_ad_post.html", dom.window.document.querySelector('body').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            let post = {}
            post.titulo = dom.window.document.querySelector('h1').textContent
            console.log(post.titulo)
            post.link = dom.window.document.querySelector('.fb-like').getAttribute('data-href')
            post.thumb = dom.window.document.querySelector('.main-img img').getAttribute('src')
            let listContent = []

            //console.log([...dom.window.document.querySelector('.entry').children])
            //return false

            let children = dom.window.document.querySelector('.thePost div.content div').children
            children = [ ...children ]
            console.log(children)

            children.forEach((el) => {
                let c = {}
                //console.log(el.tagName)

                if (el.querySelector('img') !== null) {
                    c.tipo = 'imagem'
                    c.conteudo = el.querySelector('img').getAttribute('src')
                }
                else if (el.tagName === 'P') {
                    if ((el.textContent.indexOf('[+texto original]:') === -1)) {
                        c.tipo = 'texto'
                        c.conteudo = el.textContent.replace('[+update]: ', '')
                        //console.log('p.content')
                        //console.log(c.conteudo)
                    }
                }
                else if (el.tagName === 'H2') {
                    c.tipo = 'texto-destaque'
                    c.conteudo = el.textContent
                    //console.log('p.content')
                    //console.log(c.conteudo)
                }

                if ('conteudo' in c && c.conteudo && c.conteudo !== ' ') {
                    listContent.push(c)
                }
            });

            post.listContent = listContent

            //console.log(post)

            //
            //
            //arquivoTexto
            let content = ''
            content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += "-> " + post.link
            content += "\n"
            content += "#######################################################"
            content += "\n\n"
            post.listContent.forEach(post => {
                content += "\n"
                content += post.tipo == 'texto' ? stringDivider(post.conteudo, 180, "\n") : post.conteudo
                content += "\n\n"
            })

            fs.writeFile("assets/ad/ad_post.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/ad/ad_post.json", JSON.stringify(post), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function stringDivider(str, width, spaceReplacer) {
    if (str.length > width) {
        var p = width
        for (; p > 0 && str[ p ] != ' '; p--) {
        }
        if (p > 0) {
            var left = str.substring(0, p);
            var right = str.substring(p + 1);
            return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
        }
    }
    return str;
}

switch (process.argv[ 2 ]) {
    case 'post': post();// node ol.js post https://olhardigital.com.br/ces-2019/
        break;
    default: home();
}
