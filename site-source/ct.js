import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Ct extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'bgwqn',
            'binForEachPost': '10j9b3'
        }
    }

    async home() {
        console.log('canaltech')
        let url = 'https://canaltech.com.br'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        //this.utils.dump('assets/ct/_ct_index.html', dom.window.document.querySelector('body').outerHTML)
        //const html = await this.utils.getContent('assets/ct/_ct_index.html')
        //const dom = await this.HTMLparse(html)

        let home = {}
        let listPost = []

        const parseItem = (art) => {
            let post = {}
            post.link = `${url}${art.getAttribute('href')}`
            post.titulo = art.querySelector('.item-info .title') !== null
                ? art.querySelector('.item-info .title').textContent
                : art.querySelector('h5.title').textContent
            post.conteudo = ''
            post.img = art.querySelector('[data-src-full]') !== null
                ? art.querySelector('[data-src-full]').getAttribute('data-src-full')
                : ''

            if (post.img) {
                listPost.push(post)
            }
        }

        dom.window.document.querySelectorAll('.features .item>a').forEach((art) => parseItem(art))
        dom.window.document.querySelectorAll('.features .row>a').forEach((art) => parseItem(art))
        dom.window.document.querySelectorAll('#featured-catag-columns .list-item').forEach((art) => parseItem(art))
        // Últimas Notícias
        dom.window.document.querySelectorAll('#list.listagem-colunas .list-item').forEach((art) => parseItem(art))

        home.listPost = listPost
        return home
    }

    async homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += this.utils.stringDivider(post.conteudo, 180, "\n")
            content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/ct/ct_index.txt`, content)
        this.utils.dump(`${path}assets/ct/ct_index.json`, JSON.stringify(home))
        await myJsonAPI.update(this.myJsonIds.home, home)
    }

    async post(url) {
        console.log(`canaltechPost: ${url}`)
        //let url = 'https://tecnoblog.net/1'

        //if (!isNaN(id) && id > 0) {
        //    url = `https://tecnoblog.net/${id}/`
        //}

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        //this.utils.dump('assets/ct/_ct_post.html', dom.window.document.querySelector('body').outerHTML)
        //const html = await this.utils.getContent('assets/ct/_ct_post.html')
        //const dom = await this.HTMLparse(html)

        let post = {}
        post.titulo = dom.window.document.querySelector('.timeline-wrapper h1.title').textContent
        post.link = dom.window.document.querySelector('link[rel="canonical"]') !== null
            ? dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
            : ''
        let listContent = []

        // document.querySelector('[class$="premium"]')
        let children = dom.window.document.querySelector('[class$="premium"]').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}

            if (el.tagName === 'P') {
                c.tipo = el.querySelector('strong') === null ? 'texto' : 'texto-destaque'
                c.conteudo = el.textContent
            }
            else if (el.tagName === 'H4') {
                c.tipo = 'texto-destaque'
                c.conteudo = el.textContent
            }
            else if (el.tagName === 'FIGURE') {
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('src')
            }

            if ('conteudo' in c && c.conteudo) {
                listContent.push(c)
            }
        });

        post.listContent = listContent
        return post
    }

    async postDump(post, path) {
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listContent.forEach(post => {
            content += "\n"
            content += post.tipo === 'texto' ? this.utils.stringDivider(post.conteudo, 180, "\n") : post.conteudo
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/ct/ct_post.txt`, content)
        this.utils.dump(`${path}assets/ct/ct_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = this.myJsonIds.binForEachPost

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if (binData.listBin.filter(item => item.page === post.link).length === 0) {
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Tec
