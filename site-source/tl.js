import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Tl extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': '1gdrct',//psi
            'binForEachPost': ''
        }
    }
    async home() {
        console.log('torlock')
        let url = 'http://torlock.com'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tl/index.html", dom.window.document.querySelector('body').outerHTML)
        //TORRENT NAME	                            ADDED	        SIZE	SEEDS	PEERS	HEALTH
        //Bohemian Rhapsody(2018)[ WEBRip ](1080p)	1 / 22 / 2019	2.2 GB	9346	1550	

        let home = {'listPost': []}
        //tab = dom.window.document.querySelector('.table-striped').closest('div')//.previousSibling('.table-responsive')
        let h4 = ''

        dom.window.document.querySelectorAll('.table').forEach((art) => {
            if (art.querySelector('h4') !== null) {
                h4 = art.querySelector('h4').textContent
            } else {

                art.querySelectorAll('tbody tr').forEach((tr) => {
                    let post = {}

                    post.link = 'http://torlock.com'
                    post.link += tr.querySelector('a').getAttribute('href')
                    post.titulo = tr.querySelector('a').textContent
                    //post.thumb = tr.querySelector('img').getAttribute('src')
                    post.categoria = h4

                    post.added = tr.querySelector('td:nth-of-type(2)').textContent
                    post.size = tr.querySelector('td:nth-of-type(3)').textContent
                    post.files = tr.querySelector('td:nth-of-type(4)').textContent
                    post.seeds = tr.querySelector('td:nth-of-type(5)').textContent
                    post.peers = tr.querySelector('td:nth-of-type(6)').textContent

                    home.listPost.push(post)
                })
            }
        })

        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.categoria
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/tl/tl_index.txt`, content)
        this.utils.dump(`${path}assets/tl/tl_index.json`, JSON.stringify(home))
        myJsonAPI.update('bonnq', home)
        .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        console.log('torlock-page')
        // let url = 'https://www.torlock.com/torrent/8064083/codex-resident-evil-2.html'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tl/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.thumb = ''//dom.window.document.querySelector('.content img:nth-of-type(1)').getAttribute('src')

        post.id = dom.window.document.querySelector('.nav.nav-pills a').getAttribute('href').split('/')[ 4 ]
        post.name = dom.window.document.querySelector('h4').textContent
        post.titulo = post.name
        post.size = dom.window.document.querySelector('.well .row dl:nth-of-type(4) dd').textContent
        post.link = 'http://torlock.com'
        post.link += dom.window.document.querySelector('.nav.nav-pills a').getAttribute('href')
        post.category = dom.window.document.querySelector('.well .row dl:nth-of-type(2) dd').textContent
        post.seeders = dom.window.document.querySelector('.well .row dl:nth-of-type(6) dd b:nth-of-type(1)').textContent
        post.leechers = dom.window.document.querySelector('.well .row dl:nth-of-type(6) dd :nth-of-type(2)').textContent
        post.uploadDate = dom.window.document.querySelector('.well .row dl:nth-of-type(5) dd').textContent
        post.magnetLink = dom.window.document.querySelector('a[href^="magnet"]').getAttribute('href')

        return post
    }

    async postDump(post, path) {
        let content = ''
        content += "## " + post.name.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        content += post.magnetLink

        this.utils.dump(`${path}assets/tl/tl_post.txt`, content)
        this.utils.dump(`${path}assets/tl/tl_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = 'hp4va'

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Tl
