import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Anand extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'ets6u',
            'binForEachPost': 'kwm9y'
        }
    }

    async home(pageNumber) {
        let url = 'https://www.anandtech.com/'

        if (!isNaN(pageNumber) && pageNumber > 0) {
            url = `https://www.anandtech.com/Page/${pageNumber}`
        }

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let home = {}
        /*home.pgAtual = dom.window.document.querySelector('#page-selection-area li.active').textContent
        //home.pgUltima = dom.window.document.querySelector('#page-selection-area li:nth-of-type(6)').textContent
        home.pgUltima = 1
        dom.window.document.querySelectorAll('#page-selection-area li').forEach((pag) => {
            home.pgUltima = home.pgUltima < pag.textContent && !isNaN(parseInt(pag.textContent)) ? pag.textContent : home.pgUltima
        })*/
        let listPost = []

        //<article id="post-275062" class="bloco">
        dom.window.document.querySelectorAll('.cont_box1.l_').forEach((art) => {
            let post = {}
            post.link = `${url}${art.querySelector('h2 a').getAttribute('href')}`
            post.titulo = art.querySelector('h2').textContent
            post.thumb = art.querySelector('img').getAttribute('src')
            post.preview = art.querySelector('p').textContent
            //post.conteudo = art.querySelector('.entry p').textContent
            post.conteudo = []

            //post.img = art.querySelector('.thumb img').getAttribute('data-lazy-src')

            listPost.push(post)
        });

        home.listPost = listPost
        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.preview.slice(0, 100)
            content += "\n"
            content += "\n----------\n\n"
        })
        content += `Página ${home.pgAtual} de ${home.pgUltima}`

        this.utils.dump(`${path}assets/anand/anand_index.txt`, content)
        this.utils.dump(`${path}assets/anand/anand_index.json`, JSON.stringify(home))
        myJsonAPI.update(this.myJsonIds.home, home)
          .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        //let url = 'https://www.anandtech.com/show/13688'

        //if (!isNaN(id) && id > 0) {
        //    url = `https://www.anandtech.com/show/${id}/`
        //}

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        //<article id="post-275062" class="bloco">
        const art = dom.window.document.querySelector('section.main_cont')
        let post = {}
        post.link = 'https://www.anandtech.com'+art.querySelector('.blog_top_left a').getAttribute('href')
        post.titulo = art.querySelector('h1').textContent
        post.thumb = art.querySelector('.sidepadding.review > div img').getAttribute('src')

        //post.conteudo = art.querySelector('.entry p').textContent
        post.listContent = []

        let children = art.querySelector('.articleContent').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}

            if (el.querySelector('img') !== null) {
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('src')
            }
            else if (el.querySelector('.video-container') !== null) {
                c.tipo = 'video'
                c.conteudo = el.querySelector('.video-container iframe').getAttribute('data-src')
            }
            else if (el.tagName === 'P' && el.getAttribute('class') === null && el.getAttribute('id') === null) {
                c.tipo = 'texto'
                c.conteudo = el.textContent
            }
            else if (el.tagName === 'H3') {
                c.tipo = 'texto-em-destaque'
                c.conteudo = el.textContent
            }

            if ('conteudo' in c && c.conteudo) {
                post.listContent.push(c)
            }
        })

        return post
    }

    async postDump(post, path){
        let content = ''

        content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += post.link
        content += "\n"
        //content += post.conteudo[ 0 ].conteudo.slice(0, 100)

        post.listContent.forEach(cont => {
            content += "\n"
            content += cont.tipo === 'texto' ? this.utils.stringDivider(cont.conteudo, 180, "\n") : cont.conteudo
            content += "\n"
        })

        content += "\n"
        content += "\n----------\n\n"

        this.utils.dump(`${path}assets/anand/anand_post.txt`, content)
        this.utils.dump(`${path}assets/anand/anand_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = this.myJsonIds.binForEachPost

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Anand
