import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Psi extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': '1gdrct',
            'binForEachPost': ''
        }
    }
    async home() {
        console.log('ps4iso')
        let url = 'http://ps4iso.net/most-popular-ps4-games'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/psi/index.html", dom.window.document.querySelector('body').outerHTML)

        let home = {}
        let listPost = []

        dom.window.document.querySelectorAll('.entry-content li').forEach((art) => {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')

            post.thumb = ''
            /*if (art.querySelector('.capa img') !== null && !art.querySelector('.capa img').getAttribute('src').includes('base64')) {
                post.thumb = art.querySelector('.capa img').getAttribute('src')
            }*/

            listPost.push(post)
        })

        home.listPost = listPost
        return home
    }

    homeDump(listPost, path) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            //content += post.thumb
            //content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/psi/psi_index.txt`, content)
        this.utils.dump(`${path}assets/psi/psi_index.txt`, JSON.stringify(listPost))

        // myjson.com: 1eylny
    }

    async post(url) {
        console.log('ps4clubPost')

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/psi/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h2').textContent
        post.link = dom.window.document.querySelector('meta[property="og:url"]').getAttribute('content')
        post.thumb = dom.window.document.querySelector('.logo') !== null
            ? dom.window.document.querySelector('.logo img').getAttribute('src')
            : ''

        let listLink = []

        dom.window.document.querySelector('#content .entry-content').textContent.split(' ').forEach((text) => {
            if (text.includes('http')) {
                const url = text.replace(/\t/g, "").replace(/\n/g, "")
                const l = { url, text: url, type: 'server' }
                listLink.push(l)
            }
        })

        post.listLink = listLink
        return post
    }

    postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/psi/psi_post.txt`, content)
        this.utils.dump(`${path}assets/psi/psi_post.json`, JSON.stringify(post))

        // myjson.com: 1eylny
    }
}

//module.exports = Psi
