import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Ol extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'qrvwq',
            'binForEachPost': 'g48ya'
        }
    }

    async home() {
        console.log('olhardigital')
        let url = 'https://olhardigital.com.br/'

        const html = await this.requestUrl(url)
        //const html = await this.utils.getContent('./assets/ol/_ol_index.html')
        const dom = await this.HTMLparse(html)

        //this.utils.dump("assets/ol/_ol_index.html", dom.window.document.querySelector('body').outerHTML)

        let home = {}
        let listPost = []

        //<article id="post-275062" class="bloco">
        dom.window.document.querySelectorAll('div.bloco.b-slider .slider-viewport > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = 'https:' + art.querySelector('img').getAttribute('src')
            listPost.push(post)
        })

        dom.window.document.querySelectorAll('.sidebar-content .blk-items > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            //post.link = art.querySelector('a').getAttribute('href').indexOf("//olhardigital.com.br/") === -1
            //    ? 'https://olhardigital.com.br' + art.querySelector('a').getAttribute('href')
            //    : 'https:' + art.querySelector('a').getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('img').getAttribute('data-src')

            listPost.push(post)
        })

        // Ultimos posts
        dom.window.document.querySelectorAll('.b-posts.lst_posts .blk-items > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            //post.link = art.querySelector('a').getAttribute('href').indexOf("//olhardigital.com.br/") === -1
            //    ? 'https://olhardigital.com.br' + art.querySelector('a').getAttribute('href')
            //    : 'https:' + art.querySelector('a').getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('img').getAttribute('data-src')

            listPost.push(post)
        })

        // Ultimos videos
        dom.window.document.querySelectorAll('.lst_videos .slider-viewport > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            //post.link = art.querySelector('a').getAttribute('href').indexOf("//olhardigital.com.br/") === -1
            //    ? 'https://olhardigital.com.br' + art.querySelector('a').getAttribute('href')
            //    : 'https:' + art.querySelector('a').getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('img').getAttribute('data-src')

            listPost.push(post)
        })

        // Reviews
        //items-g, items-p
        dom.window.document.querySelectorAll('.lst_materias_reviews .blk-items [class^="items-"] > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            //post.link = art.querySelector('a').getAttribute('href').indexOf("//olhardigital.com.br/") === -1
            //    ? 'https://olhardigital.com.br' + art.querySelector('a').getAttribute('href')
            //    : 'https:' + art.querySelector('a').getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('img').getAttribute('data-src')

            listPost.push(post)
        })

        // Abas
        dom.window.document.querySelectorAll('.tab-contents .blk-items > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            //post.link = art.querySelector('a').getAttribute('href').indexOf("//olhardigital.com.br/") === -1
            //    ? 'https://olhardigital.com.br' + art.querySelector('a').getAttribute('href')
            //    : 'https:' + art.querySelector('a').getAttribute('href')
            post.titulo = art.getAttribute('title')
            //post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('img').getAttribute('data-src')

            listPost.push(post)
        })

        // Mais vistos
        dom.window.document.querySelectorAll('.lst_materias_mais_vistas_semana .blk-items > a').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.getAttribute('href')
            post.titulo = art.getAttribute('title')
            post.img = art.querySelector('img') !== null && art.querySelector('img').getAttribute('data-src')
                ? art.querySelector('img').getAttribute('data-src')
                : ''

            listPost.push(post)
        })

        home.listPost = listPost
        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        console.log(`${path}assets/ol/ol_index.txt`)
        this.utils.dump(`${path}assets/ol/ol_index.txt`, content)
        this.utils.dump(`${path}assets/ol/ol_index.json`, JSON.stringify(home))
        myJsonAPI.update(this.myJsonIds.home, home)
          .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        console.log('olhardigitalPost')

        const html = await this.requestUrl(url)
        // const html = await this.utils.getContent('./assets/ol/_ol_post.html')
        const dom = await this.HTMLparse(html)

        //this.utils.dump("assets/ol/_ol_post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h1.mat-tit').textContent
        console.log(post.titulo)
        post.link = dom.window.document.querySelector('.fb-comments').getAttribute('data-href')
        post.thumb = dom.window.document.querySelector('.img-img img') !== null 
            ? dom.window.document.querySelector('.img-img img').getAttribute('src')
            : ''
        let listContent = []

        //console.log([...dom.window.document.querySelector('.entry').children])
        //return false
        listContent.push(
            { 'tipo': 'texto-destaque', 'conteudo': dom.window.document.querySelector('h3.mat-lead').textContent }
        )

        let children = dom.window.document.querySelector('.mat-txt').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}
            //console.log(el.tagName)

            if (el.querySelector('img') !== null) {
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('src')
            }
            else if (el.tagName === 'P') {
                c.tipo = 'texto'
                c.conteudo = el.textContent
                //console.log('p.content')
                //console.log(c.conteudo)
            }
            else if (el.tagName === 'H3') {
                c.tipo = 'texto-destaque'
                c.conteudo = el.textContent
                //console.log('p.content')
                //console.log(c.conteudo)
            }

            if ('conteudo' in c && c.conteudo) {
                listContent.push(c)
            }
        });

        post.listContent = listContent
        return post
    }

    async postDump(post, path) {
        /**
        * Dump no sistema de arquivos local
        */
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listContent.forEach(post => {
            content += "\n"
            content += post.tipo === 'texto' ? this.utils.stringDivider(post.conteudo, 180, "\n") : post.conteudo
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/ol/ol_post.txt`, content)
        this.utils.dump(`${path}assets/ol/ol_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = 'g48ya'

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(this.myJsonIds.binForEachPost, binData)
        }
    }
}

//module.exports = Ol
