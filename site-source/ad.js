import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Ad extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'oakpi',
            'binForEachPost': '167q0m'
        }
    }

    async home() {
        let url = 'https://adrenaline.uol.com.br/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump('assets/ad/_ad_index.html', dom.window.document.querySelector('body').outerHTML)

        let home = {}
        let listPost = []

        dom.window.document.querySelectorAll('.column ul li').forEach((art) => {
            let post = {}
            post.link = 'https:' + art.querySelector('a').getAttribute('href')
            post.id = post.link.split('/')[ 6 ]
            post.titulo = art.querySelector('a').textContent
            post.views = ''

            //
            if (art.closest('.inbox-bombando') !== null && art.closest('.inbox-bombando').tagName === 'DIV') {
                if (art.querySelector('.nome_bombando') !== null) {
                    post.titulo = art.querySelector('.nome_bombando').textContent
                    post.titulo += ` ==> ${art.querySelector('a').textContent}`
                    post.views = art.querySelector('a').textContent
                }
            }

            listPost.push(post)
        })

        home.listPost = listPost
        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/ad/ad_index.txt`, content)
        this.utils.dump(`${path}assets/ad/ad_index.json`, JSON.stringify(home))
        myJsonAPI.update(this.myJsonIds.home, home)
          .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        //this.utils.dump('assets/ad/_ad_post.html', dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h1').textContent
        post.link = dom.window.document.querySelector('.fb-like').getAttribute('data-href')
        post.thumb = dom.window.document.querySelector('.main-img img') !== null
            ? dom.window.document.querySelector('.main-img img').getAttribute('src')
            : ''
        let listContent = []

        let children = dom.window.document.querySelector('.thePost div.content div').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}

            if (el.querySelector('img') !== null) {
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('src')
            }
            else if (el.tagName === 'P') {
                if ((el.textContent.indexOf('[+texto original]:') === -1)) {
                    c.tipo = 'texto'
                    c.conteudo = el.textContent.replace('[+update]: ', '')
                }
            }
            else if (el.tagName === 'H2') {
                c.tipo = 'texto-destaque'
                c.conteudo = el.textContent
            }

            if ('conteudo' in c && c.conteudo && c.conteudo !== ' ') {
                listContent.push(c)
            }
        });

        post.listContent = listContent
        return post
    }

    async postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listContent.forEach(post => {
            content += "\n"
            content += post.tipo === 'texto' ? this.utils.stringDivider(post.conteudo, 180, "\n") : post.conteudo
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/ad/ad_post.txt`, content)
        this.utils.dump(`${path}assets/ad/ad_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = '167q0m'

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(this.myJsonIds.binForEachPost, binData)
        }
    }
}

//module.exports = Ad
