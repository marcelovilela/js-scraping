import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Sprt extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'cusi5',
            'binForEachPost': ''
        }
    }

    async home(pagina = 1) {
        let url = 'https://spartagames.net/ps4/'
        if( !isNaN(pagina) && pagina > 1 ) {
            url = `https://spartagames.net/category/ps4/page/${pagina}/`
        } else {
            pagina = 1
        }

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("../assets/sprt/index.html", dom.window.document.querySelector('body').outerHTML)

        let home = {'listPost': []}

        dom.window.document.querySelectorAll('.gp-post-item').forEach((art) => {
            if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
                let post = {}

                post.link = art.querySelector('meta[itemprop="mainEntityOfPage"]').getAttribute('content')
                post.titulo = art.querySelector('meta[itemprop="headline"]').getAttribute('content')
                post.thumb = art.querySelector('meta[itemprop="url"]').getAttribute('content')

                home.listPost.push(post)
            }
        })

        return home
    }

    homeDump(home, path, pagina = 1) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.thumb
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}./assets/sprt/indextxt/sprt_index${pagina}.txt`, content)
        this.utils.dump(`${path}./assets/sprt/indexjson/sprt_index${pagina}.json`, JSON.stringify(home))
        //myjsonapi.com | dump(ah4j2) | dump com propriedades extras para testar lista (ivc5q)
    }

    async homeMerge(path, pagina = 9) {
        var content = {listPost: []}
        
        for(let pg = 1; pg <= pagina; pg++){
            let current = await this.utils.getContent(
                `${path}assets/sprt/indexjson/sprt_index${pg}.json`
            )
            current = JSON.parse(current)

            const combinacoes = {
                0:['add', 'bg-warning'],
                1:['trending_up', 'bg-light'],
                2:['warning', 'bg-danger'],
                3:['lock', 'bg-white'],
                4:['arrow_forward', 'bg-warning'],
                5:['lock_open', 'bg-white'],
                6:['arrow_forward', 'bg-white'],
                7:['lock_open', 'bg-warning'],
                8:['trending_up', 'bg-warning'],
                9:['add', 'bg-white']
            }
            current.listPost.forEach(element => {
                const random = Math.floor((Math.random() * 10)); 
                element.icon = combinacoes[random][0]
                element.spotLight = combinacoes[random][1]
                element.spotLightMedia = combinacoes[random][1]
                element.thumb = 'https://store.playstation.com/store/api/chihiro/00_09_000/container/BR/pt/999/UP9000-CUSA10237_00-HRZCE00000000000/1553559319000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000'
            })

            //console.log(Object.keys(current))
            content.listPost = [...content.listPost, ...current.listPost]
            this.utils.dump(`${path}assets/sprt/todos.json`, JSON.stringify(content))
        }
    }

    async post(url) {
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        //this.utils.dump("assets/sprt/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h1').textContent
        post.link = dom.window.document.querySelector('meta[itemprop="mainEntityOfPage"]').getAttribute('content')
        post.thumb = dom.window.document.querySelector('meta[itemprop="url"]').getAttribute('content')

        let listLink = []
        const serverList = [ 'www.zippyshare.com' ]

        // <li class="tab-pane " id="ert_pane1-4"><strong>Opções de Downloads:</strong>
        dom.window.document.querySelectorAll('.tab-content a').forEach((link) => {
            let l = {}

            if (link.getAttribute('href').includes('magnet')) {
                const magnet = link.getAttribute('href').replace('https://curtlink.com/?l=', "")
                l = { url: magnet, text: link.textContent, type: 'magnet' }
                listLink.push(l)
            }

            const site = link.getAttribute('href').split('/')[ 2 ]

            if (serverList.indexOf(site) > -1) {
                l = { url: link.getAttribute('href'), text: link.textContent, type: 'server' }
                listLink.push(l)
            }
        })

        post.listLink = listLink
        return post
    }

    postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/sprt/sprt_post.txt`, content)
        this.utils.dump(`${path}assets/sprt/sprt_post.json`, JSON.stringify(post))
    }
}

//module.exports = Sprt
