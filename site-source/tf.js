import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Tf extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': '1gdrct',//psi
            'binForEachPost': ''
        }
    }
    async home() {
        console.log('thef')
        let url = 'https://torrentfilmes.net/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tf/index.html", dom.window.document.querySelector('body').outerHTML)

        let listPost = []

        dom.window.document.querySelectorAll('#meio .item').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')
            post.thumb = art.querySelector('img').getAttribute('src')

            listPost.push(post)
            //}
        })

        // Lançamentos</strong> em destaque
        dom.window.document.querySelectorAll('#home-destaques .item').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')
            post.thumb = art.querySelector('img').getAttribute('src')

            listPost.push(post)
            //}
        })

        return {listPost}
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.thumb
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/tf/tf_index.txt`, content)
        this.utils.dump(`${path}assets/tf/tf_index.json`, JSON.stringify(home))
        myJsonAPI.update('h5ufa', home)
          .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        console.log('thefPost')
        // let url = 'torrentfilmes.net/johnny-english-3-0-dublado'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tf/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('meta[property="og:title"]').getAttribute('content')
        post.link = dom.window.document.querySelector('meta[property="og:url"]').getAttribute('content')
        post.thumb = dom.window.document.querySelector('.content img:nth-of-type(1)').getAttribute('src')

        let listLink = []
        //let h2 = ''

        dom.window.document.querySelectorAll('.content a').forEach((link) => {
            let l = {}
            let text = link.closest('.bt-down').textContent
            text = text.replace(link.textContent, "")
            l = { url: link.getAttribute('href'), text, type: 'server' }
            listLink.push(l)
            //}
        })

        post.listLink = listLink
        return post
    }

    postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.text
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/tf/tf_post.txt`, content)
        this.utils.dump(`${path}assets/tf/tf_post.json`, JSON.stringify(post))
    }
}

//module.exports = Tf
