import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Prmc extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
    }

    async home() {
        console.log('blog')
        let url = 'http://promocaogames.blogspot.com.br'

        //topicos do forum
        //post-title entry-title
        //Dead by Daylight por R$ 29,90

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/prmc/prmc_index.html", dom.window.document.documentElement.outerHTML)

        let listPost = []

        dom.window.document.querySelectorAll('.post-outer .post').forEach((art) => {
            //let art = dom.window.document.querySelector('.post-outer .post')
            let post = {}
            post.titulo = art.querySelector('h3.post-title').textContent
            console.log(post.titulo)
            post.link = art.querySelector('h3.post-title a').getAttribute('href')
            post.data = art.querySelector('abbr').getAttribute('title')
            post.autor = art.querySelector('.g-profile').textContent
            post.comentarios = parseInt(art.querySelector('.comment-link').textContent)
            // art.querySelector('.comment-link').textContent.split(' ')[0]

            //
            //
            post.conteudoTextual = art.querySelector('.post-body').textContent
            post.conteudos = []

            if (art.querySelector('table') === null) {//inicio-nao-eh-tabela
                // pega o html
                const conteudoHtml = art.querySelector('.post-body').outerHTML

                // divide em linhas, para analisar individualmente
                const linhaHtml = conteudoHtml.split("\n")

                linhaHtml.forEach(async (h) => {
                    //console.log('linha html')
                    //console.log(h)
                    // identifica

                    const dom = await this.HTMLparse(h, {})
                    let conteudo = {}

                    if (h.includes('a href') && h.includes('R$')) {
                        //console.log(dom.window.document.children[0].outerHTML)
                        //
                        //inicializa
                        conteudo.tipo = 'direcionamento'

                        //
                        //direcionamento propriamente dito
                        conteudo.url = dom.window.document.querySelector('a').getAttribute('href')

                        //
                        //nome/titulo para identificacao
                        conteudo.titulo = dom.window.document.querySelector('b').textContent

                        //
                        //valor
                        //recupera todo o texto, e...
                        conteudo.valor = dom.window.document.children[ 0 ].textContent
                            //seleciona o que vem apos o titulo
                            .split(conteudo.titulo)[ 1 ]

                        //
                        //caso identifique algo alem de link+titulo+preco, entao preenche o comentario
                        conteudo.comentario = ''
                        console.log(h.indexOf('<a'))
                        if (h.indexOf('<a') !== 3) {
                            conteudo.comentario = dom.window.document.children[ 0 ].textContent
                        }
                        post.conteudos.push(conteudo)
                    } else {
                        conteudo.tipo = 'texto'
                        conteudo.titulo = dom.window.document.children[ 0 ].textContent
                        if (conteudo.titulo) {
                            post.conteudos.push(conteudo)
                        }
                    }
                })

            }//fim-nao-eh-tabela
            else {
                art.querySelectorAll('table').forEach((table) => {
                    //let table = art.querySelector('table')
                    let conteudo = {}
                    conteudo.tipo = 'tabela'
                    conteudo.linhas = []
                    table.querySelectorAll('tr').forEach((tr) => {
                        //console.log(tr.querySelector('td:nth-of-type(1)').outerHTML)
                        //return false
                        //
                        //inicializa
                        if (tr.querySelector('td:nth-of-type(1) a') !== null) {
                            let linha = {}
                            linha.tipo = 'linha de tabela'

                            //
                            //direcionamento propriamente dito
                            linha.url = tr.querySelector('td:nth-of-type(1) a').getAttribute('href')

                            //
                            //nome/titulo para identificacao
                            linha.titulo = tr.querySelector('td:nth-of-type(1) a').textContent
                            console.log(linha.titulo)

                            linha.valor = tr.querySelector('td:nth-of-type(2)').textContent
                            console.log(linha.valor)
                            linha.valorUS = tr.querySelector('td:nth-of-type(3)').textContent
                            console.log(linha.valorUS)
                            linha.porcentual = tr.querySelector('td:nth-of-type(4)').textContent
                            console.log(linha.porcentual)
                            linha.comentario = tr.querySelector('td:nth-of-type(5)').textContent
                            console.log(linha.comentario)
                            linha.tipoo = '=>' + tr.querySelector('td:nth-of-type(6)').textContent
                            console.log(linha.tipoo)
                            conteudo.linhas.push(linha)
                        }
                    })
                    post.conteudos.push(conteudo)
                })

            }
            listPost.push(post)
        });

        return listPost
    }

    homeDump(listPost, path) {
        //
        //
        //arquivoTexto
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += JSON.stringify(post.conteudos[ 0 ])
            content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/prmc/prmc_index.txt`, content)
        this.utils.dump(`${path}assets/prmc/prmc_index.json`, JSON.stringify(listPost))
    }
}

//module.exports = Prmc
