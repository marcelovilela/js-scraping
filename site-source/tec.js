import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Tec extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'xgv52',
            'binForEachPost': '157i06'
        }
    }

    async home() {
        console.log('tecnoblog')
        let url = 'https://tecnoblog.net/'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let home = {}
        let listPost = []

        //<article id="post-275062" class="bloco">
        dom.window.document.querySelectorAll('article.bloco').forEach((art) => {
            let post = {}
            post.link = art.querySelector('h2 a').getAttribute('href')
            post.titulo = art.querySelector('h2').textContent
            post.conteudo = art.querySelector('.entry p').textContent
            post.img = art.querySelector('.thumb img').getAttribute('data-lazy-src')

            listPost.push(post)
        });

        home.listPost = listPost
        return home
    }

    async homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += this.utils.stringDivider(post.conteudo, 180, "\n")
            content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/tec/tec_index.txt`, content)
        this.utils.dump(`${path}assets/tec/tec_index.json`, JSON.stringify(home))
        await myJsonAPI.update(this.myJsonIds.home, home)
    }

    async post(url) {
        console.log(`tecnoblogPost: ${url}`)
        //let url = 'https://tecnoblog.net/1'

        //if (!isNaN(id) && id > 0) {
        //    url = `https://tecnoblog.net/${id}/`
        //}

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let post = {}
        post.titulo = dom.window.document.querySelector('article#post h1.title').textContent
        post.link = dom.window.document.querySelector('article#post h1 a').getAttribute('href')
        let listContent = []

        let children = dom.window.document.querySelector('.entry').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}

            if (el.tagName === 'P') {
                c.tipo = 'texto'
                c.conteudo = el.textContent
            }
            else if (el.tagName === 'IMG') {
                c.tipo = 'imagem'
                c.conteudo = el.getAttribute('data-lazy-src')
            }

            if ('conteudo' in c && c.conteudo) {
                listContent.push(c)
            }
        });

        post.listContent = listContent
        return post
    }

    async postDump(post, path){
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listContent.forEach(post => {
            content += "\n"
            content += post.tipo === 'texto' ? this.utils.stringDivider(post.conteudo, 180, "\n") : post.conteudo
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/tec/tec_post.txt`, content)
        this.utils.dump(`${path}assets/tec/tec_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = this.myJsonIds.binForEachPost

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Tec
