import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'

export default class Wvd extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
    }
    async home() {
        console.log('wolverdon')
        let url = 'https://wolverdonfilmes.org'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/wvd/index.html", dom.window.document.querySelector('body').outerHTML)

        let listPost = []

        dom.window.document.querySelectorAll('#content article.type-post').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').textContent
            post.thumb = art.querySelector('img').getAttribute('src')

            listPost.push(post)
            //}
        })

        // +BAIXADOS
        /*dom.window.document.querySelectorAll('.wpp-list:nth-of-type(1) li').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}
 
            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')
            post.thumb = art.querySelector('img.wpp-thumbnail').getAttribute('src')
 
            listPost.push(post)
            //}
        })*/

        return listPost
    }

    homeDump(listPost, path) {
        //
        //
        //arquivoTexto
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.thumb
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/wvd/wvd_index.txt`, content)
        this.utils.dump(`${path}assets/wvd/wvd_index.json`, JSON.stringify(listPost))
    }

    async post(url) {
        console.log('wolverdonPost')

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/wvd/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('meta[property="og:title"]').getAttribute('content')
        post.link = dom.window.document.querySelector('meta[property="og:url"]').getAttribute('content')
        post.thumb = dom.window.document.querySelector('#content img').getAttribute('src')

        let listLink = []
        let h2 = ''
        const serverList = [ 'www.zippyshare.com' ]

        // <li class="tab-pane " id="ert_pane1-4"><strong>Opções de Downloads:</strong>
        dom.window.document.querySelectorAll('#content p').forEach((link) => {
            let l = {}

            if (link.querySelector('b') !== null) {
                h2 = `[[${link.querySelector('b').textContent}]] `
            } else if (link.querySelector('a') != null) {
                const linhaLista = link.outerHTML.split("\n")

                linhaLista.forEach((linha) => {
                    /*const el = JSDOM.fragment(linha)

                    if (el.querySelector('a:nth-of-type(1)') !== null) {
                        const magnet = el.querySelector('a').getAttribute('href')
                        let text = h2
                        text += el.textContent.split(' |')[ 0 ]
                        l = { url: magnet, text, type: 'magnet' }
                        listLink.push(l)
                    } else {
                        return false
                    }

                    if (el.querySelector('a:nth-of-type(2)') !== null) {
                        const magnet = el.querySelector('a:nth-of-type(2)').getAttribute('href')
                        let text = h2
                        text += el.textContent.split(': ')[ 0 ]
                        text += ': ' + el.querySelector('a:nth-of-type(2)').textContent
                        l = { url: magnet, text, type: 'magnet' }
                        listLink.push(l)
                    }

                    if (el.querySelector('a:nth-of-type(3)') !== null) {
                        const magnet = el.querySelector('a:nth-of-type(3)').getAttribute('href')
                        let text = h2
                        text += el.textContent.split(': ')[ 0 ]
                        text += ': ' + el.querySelector('a:nth-of-type(3)').textContent
                        l = { url: magnet, text, type: 'magnet' }
                        listLink.push(l)
                    }

                    if (el.querySelector('a:nth-of-type(4)') !== null) {
                        const magnet = el.querySelector('a:nth-of-type(4)').getAttribute('href')
                        let text = h2
                        text += el.textContent.split(': ')[ 0 ]
                        text += ': ' + el.querySelector('a:nth-of-type(4)').textContent
                        l = { url: magnet, text, type: 'magnet' }
                        listLink.push(l)
                    }*/
                })
            }
        })

        post.listLink = listLink
        return post
    }

    postDump(post, path) {
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.text
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/wvd/wvd_post.txt`, content)
        this.utils.dump(`${path}assets/wvd/wvd_json.txt`, JSON.stringify(post))
    }
}

//module.exports = Wvd
