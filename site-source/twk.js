import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Twk extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': 'psdfq',
            'binForEachPost': '188t6u'
        }
    }

    async home(pageNumber) {
        console.log('tweaktown')
        let url = 'https://www.tweaktown.com/'

        if (!isNaN(pageNumber) && pageNumber > 0) {
            url = `https://www.tweaktown.com/index${pageNumber}.html`
        }

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        let home = {}
        home.pgAtual = dom.window.document.querySelector('#page-selection-area li.active').textContent
        //home.pgUltima = dom.window.document.querySelector('#page-selection-area li:nth-of-type(6)').textContent
        home.pgUltima = 1
        //comentado pq react reclama
        //Missing radix parameter radix
        /*dom.window.document.querySelectorAll('#page-selection-area li').forEach((pag) => {
            home.pgUltima = home.pgUltima < pag.textContent && !isNaN(parseInt(pag.textContent)) 
            ? pag.textContent : home.pgUltima
            //console.log(`${pag.textContent} > ${home.pgUltima} ... ${home.pgUltima}`)
        })*/
        let listPost = []

        //<article id="post-275062" class="bloco">
        dom.window.document.querySelectorAll('article.latestpost').forEach((art) => {
            let post = {}
            post.link = art.querySelector('h2 a').getAttribute('href').indexOf('http') === -1
                ? 'https://tweaktown.com/' + art.querySelector('h2 a').getAttribute('href')
                : art.querySelector('h2 a').getAttribute('href')
            post.titulo = art.querySelector('h2').textContent
            //post.conteudo = art.querySelector('.entry p').textContent
            post.conteudo = []

            let children = art.querySelector('#KonaBody').children
            children = [ ...children ]

            children.forEach((el) => {
                let c = {}
                //console.log(el.tagName)

                if (el.querySelector('img') !== null) {
                    //console.log('el img')
                    //console.log(el)
                    c.tipo = 'imagem'
                    c.conteudo = el.querySelector('img').getAttribute('data-src')
                }
                else if (el.querySelector('.video-container') !== null) {
                    //console.log('el img')
                    //console.log(el)
                    c.tipo = 'video'
                    c.conteudo = el.querySelector('.video-container iframe').getAttribute('data-src')
                }
                else if (el.tagName === 'P' && el.getAttribute('class') === null && el.getAttribute('id') === null) {
                    c.tipo = 'texto'
                    c.conteudo = el.textContent
                    //console.log('p.content')
                    //console.log(c.conteudo)
                }

                if ('conteudo' in c && c.conteudo) {
                    post.conteudo.push(c)
                }
            })

            //post.img = art.querySelector('.thumb img').getAttribute('data-lazy-src')

            listPost.push(post)
        });

        home.listPost = listPost
        return home
    }

    homeDump(home, path) {
        let content = ''
        home.listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.conteudo[ 0 ].conteudo.slice(0, 100)
            content += "\n"
            content += "\n----------\n\n"
        })
        content += `Página ${home.pgAtual} de ${home.pgUltima}`

        this.utils.dump(`${path}assets/twk/twk_index.txt`, content)
        this.utils.dump(`${path}assets/twk/twk_index.json`, JSON.stringify(home))
        myJsonAPI.update(this.myJsonIds.home, home)
        .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        console.log('tweaktown-post')
        //let url = 'https://www.tweaktown.com/news/64597'

        //if (!isNaN(id) && id > 0) {
        //    url = `https://www.tweaktown.com/news/${process.argv[ 3 ]}/`
        //}

        /**
        * Conteudo deixou de vir no carregamento da pagina, provavelmente eh recuperado por xhr...
        */
        const html = await this.requestUrl(url)
        //const html = await this.utils.getContent('./assets/twk/_twk_post.html')
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/twk/twk_post.html", dom.window.document.querySelector('body').outerHTML)

        //<article id="post-275062" class="bloco">
        const art = dom.window.document.querySelector('article#main-content')
        let post = {}
        post.link = dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
        post.thumb = dom.window.document.querySelector('meta[property="og:image"]').getAttribute('content')
        post.titulo = dom.window.document.querySelector('title').textContent
        //post.conteudo = art.querySelector('.entry p').textContent
        post.listContent = []

        let children = art.querySelector('#KonaBody').children
        children = [ ...children ]

        children.forEach((el) => {
            let c = {}
            //console.log(el.tagName)

            if (el.querySelector('img') !== null) {
                //console.log('el img')
                //console.log(el)
                c.tipo = 'imagem'
                c.conteudo = el.querySelector('img').getAttribute('data-src')
            }
            else if (el.querySelector('.video-container') !== null) {
                //console.log('el img')
                //console.log(el)
                c.tipo = 'video'
                c.conteudo = el.querySelector('.video-container iframe').getAttribute('data-src')
            }
            else if (el.tagName === 'P' && el.getAttribute('class') === null && el.getAttribute('id') === null) {
                c.tipo = 'texto'
                c.conteudo = el.textContent
                //console.log('p.content')
                //console.log(c.conteudo)
            }

            if ('conteudo' in c && c.conteudo) {
                post.listContent.push(c)
            }
        })

        return post
    }

    async postDump(post, path) {
        let content = ''

        content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += post.link
        content += "\n"
        //content += post.conteudo[ 0 ].conteudo.slice(0, 100)

        post.listContent.forEach(cont => {
            content += "\n"
            content += cont.tipo === 'texto' ? this.utils.stringDivider(cont.conteudo, 180, "\n") : cont.conteudo
            content += "\n"
        })

        content += "\n"
        content += "\n----------\n\n"

        this.utils.dump(`${path}assets/twk/twk_post.txt`, content)
        this.utils.dump(`${path}assets/twk/twk_post.json`, JSON.stringify(post))
    
        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = this.myJsonIds.binForEachPost

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)
        
        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }

    }
}

//module.exports = Twk
