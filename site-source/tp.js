
import Scrap from './../helpers/Scrap'
import Utils from './../helpers/Utils'
import myJsonAPI from 'myjson-api'

export default class Tp extends Scrap {
    constructor() {
        super()
        this.utils = new Utils()
        this.myJsonIds = {
            'home': '1gdrct',//psi
            'binForEachPost': ''
        }
    }

    async home() {
        console.log('thep')
        let url = 'https://www.thepiratefilmes.biz'

        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tp/index.html", dom.window.document.querySelector('body').outerHTML)

        let listPost = []

        dom.window.document.querySelectorAll('.blog-view.type-post').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').textContent
            post.thumb = art.querySelector('img.alignleft').getAttribute('src')
            post.thumb = !post.thumb.includes('https://') ? 'https:'+post.thumb : post.thumb
            //post.thumb = art.querySelector('img.alignleft.size-full').getAttribute('src')

            listPost.push(post)
            //}
        })

        // +BAIXADOS
        dom.window.document.querySelectorAll('.wpp-list:nth-of-type(1) li').forEach((art) => {
            //if (art.querySelector('meta[itemprop="mainEntityOfPage"]') !== null) {
            let post = {}

            post.link = art.querySelector('a').getAttribute('href')
            post.titulo = art.querySelector('a').getAttribute('title')
            post.thumb = art.querySelector('img.wpp-thumbnail').getAttribute('src')
            post.thumb = !post.thumb.includes('https://') ? 'https:'+post.thumb : post.thumb

            listPost.push(post)
            //}
        })

        return listPost
    }

    homeDump(listPost, path) {
        let content = ''
        listPost.forEach(post => {
            content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += post.link
            content += "\n"
            content += post.thumb
            content += "\n"
            //content += stringDivider(post.conteudo, 180, "\n")
            //content += "\n"
            content += "\n----------\n\n"
        })

        this.utils.dump(`${path}assets/tp/tp_index.txt`, content)
        this.utils.dump(`${path}assets/tp/tp_index.json`, JSON.stringify(listPost))
        myJsonAPI.update('conye', listPost)
          .then((updatedJSON) => console.log(updatedJSON))
    }

    async post(url) {
        const html = await this.requestUrl(url)
        const dom = await this.HTMLparse(html)

        this.utils.dump("assets/tp/post.html", dom.window.document.querySelector('body').outerHTML)

        let post = {}
        post.titulo = dom.window.document.querySelector('h4').textContent
        post.link = dom.window.document.querySelector('.fb-comments').getAttribute('data-href')
        post.thumb = dom.window.document.querySelector('#header-logo img').getAttribute('src')

        //console.log(post)
        let listLink = []
        let h2 = ''
        //const serverList = [ 'www.zippyshare.com' ]

        // <li class="tab-pane " id="ert_pane1-4"><strong>Opções de Downloads:</strong>
        dom.window.document.querySelectorAll('.entry-content h2, .entry-content a').forEach((link) => {
            let l = {}

            if (link.tagName === 'H2') {
                h2 = link.textContent
            }
            else if (link.getAttribute('href').includes('magnet')) {
                const magnet = link.getAttribute('href')
                let text = link.closest('p') !== null && link.closest('p').querySelector('b') !== null
                    ? link.closest('p').querySelector('b').textContent
                    : ''
                //console.log(link.closest('b'))
                text += h2 + link.textContent
                l = { url: magnet, text, type: 'magnet' }
                listLink.push(l)
            }
            else {
                l = { url: link.getAttribute('href'), text: link.textContent, type: 'server' }
                listLink.push(l)
            }
        })

        post.listLink = listLink
        return post
    }

    async postDump(post, path) {
        let content = ''
        content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
        content += "\n"
        content += "-> " + post.link
        content += "\n"
        content += "#######################################################"
        content += "\n\n"
        post.listLink.forEach(post => {
            content += "\n"
            content += post.text
            content += "\n"
            content += post.url
            content += "\n\n"
        })

        this.utils.dump(`${path}assets/tp/tp_post.txt`, content)
        this.utils.dump(`${path}assets/tp/tp_post.json`, JSON.stringify(post))

        /**
         * Dump em servico online: api.myjson.com
         */
        const binId = 'f4r92'

        // Recupera a lista de dumps
        let binData = await myJsonAPI.get(binId)

        // Caso nao encontre o respectivo dump, entao cria
        if(binData.listBin.filter(item => item.page === post.link).length === 0){
            const created = await myJsonAPI.create(post)

            binData[ 'listBin' ].push({
                page: post.link,
                bin: created.uri
            })

            await myJsonAPI.update(binId, binData)
        }
    }
}

//module.exports = Tp
