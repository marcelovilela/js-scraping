const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const fs = require('fs');

function home() {
    console.log('tecmundo')
    let url = 'https://www.tecmundo.com.br/'

    JSDOM.fromURL(url)
        .then(dom => {
            //console.log('titulo da pagina')
            //console.log(dom.window.document.documentElement.outerHTML)
            console.log(dom.window.document.querySelector('h2').textContent)
            //window.document.documentElement.outerHTML
            //return false
            //console.log('titulo de uma postagem')
            //console.log(dom.window.document.querySelector('.discussionListItem h3').textContent)

            let listPost = []

            //<article id="post-275062" class="bloco">
            dom.window.document.querySelectorAll('.nzn-main a').forEach((art) => {
                let post = {}
                post.link = art.getAttribute('href')
                post.titulo = art.querySelector('h2').textContent
                //post.conteudo = art.querySelector('.entry p').textContent
                post.img = art.querySelector('img').getAttribute('data-lazy-src')

                listPost.push(post)
            });

            //console.log(listPost)

            //
            //
            //arquivoTexto
            let content = ''
            listPost.forEach(post => {
                content += post.titulo.replace(/\t/g, "").replace(/\n/g, "")
                content += "\n"
                content += post.link
                content += "\n"
                //content += stringDivider(post.conteudo, 180, "\n")
                //content += "\n"
                content += "\n----------\n\n"
            })

            fs.writeFile("assets/tecm/tecm_index.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/tecm/tecm_index.json", JSON.stringify(listPost), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function post() {
    console.log('tecmundoPost')
    let url = 'https://www.tecmundo.com.br/'

    if (process.argv.length >= 3) {//138169
        url = `https://www.tecmundo.com.br/${process.argv[ 3 ]}/`
    }

    JSDOM.fromURL(url, { runScripts: "dangerously" })
        .then(dom => {
            //console.log('titulo da pagina')
            //console.log(dom.window.document.documentElement.outerHTML)
            console.log(dom.window.document.querySelector('h1').textContent)

            //window.document.documentElement.outerHTML
            //return false
            //console.log('titulo de uma postagem')
            //console.log(dom.window.document.querySelector('.discussionListItem h3').textContent)

            let post = {}
            post.titulo = dom.window.document.querySelector('h1').textContent
            post.link = dom.window.document.querySelector('link[rel="canonical"]').getAttribute('href')
            //console.log('post.link')
            //console.log(post.link)
            let listContent = []

            fs.writeFile("assets/tecm/_post.html", dom.window.document.querySelector('html').outerHTML, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            //console.log([...dom.window.document.querySelector('.entry').children])
            //return false

            //class="article-text highlight p402_premium"
            let children = dom.window.document.querySelector('.article-text').children
            children = [...children]

            children.forEach((el) => {
                let c = {}
                //console.log(el.tagName)

                if (el.querySelector('img') !== null) {
                    //console.log('el img')
                    //console.log(el)
                    c.tipo = 'imagem'
                    c.conteudo = el.querySelector('img').getAttribute('src')
                }
                else if (el.querySelector('.wccf_related_post') !== null) {
                    //console.log('wccf_related_post')
                }
                else if (el.tagName === 'DIV') {
                    //console.log('DIV')
                }
                else if (el.querySelector('.video-container') !== null) {
                    //console.log('el img')
                    //console.log(el)
                    c.tipo = 'video'
                    c.conteudo = el.querySelector('.video-container iframe').getAttribute('data-src')
                }
                else if (el.tagName === 'P' && el.getAttribute('class') === null && el.getAttribute('id') === null) {
                    c.tipo = 'texto'
                    c.conteudo = el.textContent
                    //console.log('p.content')
                    //console.log(c.conteudo)
                }
                else if (el.tagName === 'H2') {
                    c.tipo = 'texto-destaque'
                    c.conteudo = el.textContent
                    //console.log('p.content')
                    //console.log(c.conteudo)
                }

                if ('conteudo' in c && c.conteudo) {
                    listContent.push(c)
                }
            });

            post.listContent = listContent

            //console.log(post)

            //
            //
            //arquivoTexto
            let content = ''
            content += "## " + post.titulo.replace(/\t/g, "").replace(/\n/g, "")
            content += "\n"
            content += "-> " + post.link
            content += "\n"
            content += "#######################################################"
            content += "\n\n"
            post.listContent.forEach(post => {
                content += "\n"
                content +=  post.tipo == 'texto' ? stringDivider(post.conteudo, 180, "\n") : post.conteudo
                content += "\n\n"
            })

            fs.writeFile("assets/tecm/tecm_post.txt", content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })

            fs.writeFile("assets/tecm/tecm_post.json", JSON.stringify(post), function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
        )
}

function stringDivider(str, width, spaceReplacer) {
    if (str.length > width) {
        var p = width
        for (; p > 0 && str[ p ] != ' '; p--) {
        }
        if (p > 0) {
            var left = str.substring(0, p);
            var right = str.substring(p + 1);
            return left + spaceReplacer + stringDivider(right, width, spaceReplacer);
        }
    }
    return str;
}

switch (process.argv[ 2 ]) {
    case 'post': post();// node index2.js post 275062
        break;
    default: home();
}
