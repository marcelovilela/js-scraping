const fs = require('fs')
const myJsonAPI = require('myjson-api')

/*const dom = new JSDOM(``, {
    url: "https://adrenaline.uol.com.br/forum/forums/retro.224/",
    referrer: "https://adrenaline.uol.com.br/forum/forums/retro.224/",
    contentType: "text/html",
    includeNodeLocations: true,
    storageQuota: 10000000
});

//console.log(dom.window.document)*/
export default class Scrap {

    /**
     * Dispara requisicao, retorna estrutura html da pagina
     * node v6.11.2 nao aceita async/await
     */
    async requestUrl (url) {
        // Objeto associando nome da lib a metodo dessa classe
        const libsAvailable = {
            'isomorphic-fetch': 'requestUrl_isomorphicFetch', 
            'jsdom':'requestUrl_jsdom', 
            'node-fetch': 'requestUrl_nodeFetch'
        }
        
        // Verifica a lib informada
        const callFunction = global.libRequest in libsAvailable
            ? libsAvailable[global.libRequest] 
            : 'requestUrl_nodeFetch'

        //const htmlString = await this.requestUrl_libName(url)
        const htmlString = await this[callFunction](url)
        return htmlString
    }

    async requestUrl_isomorphicFetch (url) {
        //if(url.includes('adrenaline.uol.com.br')){
        require('es6-promise').polyfill();

        if ('/adrenaline.uol.com.br/i'.match(url) !== null) {
            throw(`Indisponivel ${url}`)
        }

        const fetchIso = require('isomorphic-fetch')
        //

        const res1 = await fetchIso('//cors-anywhere.herokuapp.com/'+url, {origin: window.location.protocol + '//' + window.location.host})
        const res2 = await res1.text()
        return res2
    }

    async requestUrl_jsdom(url) {alert('jsdom')
        const jsdom = require('jsdom')
        const { JSDOM } = jsdom
        //

        const dom = await JSDOM
            .fromURL(url)
            .then(dom => { return dom })
        const htmlString = dom.window.document.querySelector('html').outerHTML
        return htmlString
    }

    async requestUrl_nodeFetch (url) {
        const fetchNode = require('node-fetch')
        //

        const response = await fetchNode(url, { mode: 'opaque' })
        const htmlString = await response.text()
        return htmlString
    }

    /**
     * Converte a string html em objeto
     */
    async HTMLparse(hmtlString) {
        // Objeto associando nome da lib a metodo dessa classe
        const libsAvailable = {
            'cheerio': 'HTMLparse_cheerio', 
            'jsdom': 'HTMLparse_jsdom', 
            'parse5': 'HTMLparse_parse5'
        }
            
        // Verifica a lib informada
        const callFunction = global.libHTMLparse in libsAvailable
            ? libsAvailable[global.libHTMLparse] 
            : 'HTMLparse_jsdom'
            
        //const html = this.HTMLparse_libName(hmtlString)
        const html = this[callFunction](hmtlString)
        return html
    }

    async HTMLparse_cheerio(hmtlString) {
        const cheerio = require('cheerio')
        //
        
        var $ = cheerio.load(hmtlString)
        return $
        //Copy the functionality of outerHTML #54
        //https://github.com/cheeriojs/cheerio/issues/54
        //$(this).html()*/
    }    

    async HTMLparse_jsdom(hmtlString) {
        const jsdom = require('jsdom')
        const { JSDOM } = jsdom
        //

        return new JSDOM(hmtlString, {})
    }

    async HTMLparse_parse5(hmtlString) {
        const parse5 = require('parse5');
        //

        const document = parse5.parse('<!DOCTYPE html><html><head></head><body>Hi there!</body></html>');
        return document
    }

    async getBin(binId) {
        const bin = await myJsonAPI.get(binId)
        return bin
    }

    stringDivider(str, width, spaceReplacer) {
        if (str.length > width) {
            var p = width
            for (; p > 0 && str[ p ] !== ' '; p--) {
            }
            if (p > 0) {
                var left = str.substring(0, p);
                var right = str.substring(p + 1);
                return left + spaceReplacer + this.stringDivider(right, width, spaceReplacer);
            }
        }
        return str;
    }

    dump(path, content) {
        fs.writeFile(path, content, function (err) {
            if (err) {
                return //console.log(err);
            }
            //console.log(`The file was saved!`);
        })
    }

    _getFl4shC4rd(){
        return `
        dom.window.document.querySelector('body').outerHTML
        ---
        art.closest('.inbox-bombando') !== null && art.closest('.inbox-bombando').tagName === 'DIV'
        ---
        let children = dom.window.document.querySelector('.thePost div.content div').children
        [ ...children ].forEach((el) => {
        ---
        el.tagName
        `
    }
}

////module.exports = Scrap
