const fs = require('fs')
const fsPromises = fs.promises

export default class Utils {
    stringDivider(str, width, spaceReplacer) {
        if (str.length > width) {
            var p = width
            for (; p > 0 && str[ p ] !== ' '; p--) {
            }
            if (p > 0) {
                var left = str.substring(0, p);
                var right = str.substring(p + 1);
                return left + spaceReplacer + this.stringDivider(right, width, spaceReplacer);
            }
        }
        return str;
    }

    dump(path, content) {
        if(typeof fs.writeFile === 'function'){
            fs.writeFile(path, content, function (err) {
                if (err) {
                    return //console.log(err);
                }
                //console.log(`The file was saved!`);
            })
        }
    }

    async _getContent(path) {
        let dataReturn = ''
        if(typeof fs.readFile === 'function'){
            const f = await fs.readFile(path, 'utf8', (err, data) => {
                if ( err ) {
                  return //console.log(err)
                }
                return data.toString()
            })
            console.log('retorno no getContent')
            console.log(f)
            return dataReturn
        }
        return dataReturn
    }

    // https://stackoverflow.com/questions/40593875/using-filesystem-in-node-js-with-async-await
    // Native async await style fs functions since version 10 [Experimental]
    async getContent(path) {
        try {
          const data = await fsPromises.readFile(path);
          return data.toString()
        } catch (err) {
          console.error('Error occured while reading directory!', err);
        }
      }

    _lembretes() {
        return `
        const path = process.mainModule.paths[ 0 ].split('node_modules')[ 0 ]
        ---
        dom.window.document.querySelector('body').outerHTML
        ---
        art.closest('.inbox-bombando') !== null && art.closest('.inbox-bombando').tagName === 'DIV'
        ---
        let children = dom.window.document.querySelector('.thePost div.content div').children
        [ ...children ].forEach((el) => {
        `
    }
}
