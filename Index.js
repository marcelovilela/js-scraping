//import scapperGroupedBySite from './../../src_scrap/scrap_v5_jsdom_integrated_react_or_cli'
//export default scapperGroupedBySite


/**
 * Engloba os scrappers e assets
 * De forma que estejam disponiveis para um App
 */

import homeAd from './assets/ad/ad_index.json'
import postAd from './assets/ad/ad_post.json'
import scrapAd from './site-source/ad'
//
import homeAdr from './assets/adr/adr_index.json'
import forumAdr from './assets/adr/adr_forums.json'
import postAdr from './assets/adr/adr_post.json'
import scrapAdr from './site-source/adr'
//
import homeAnand from './assets/anand/anand_index.json'
import postAnand from './assets/anand/anand_post.json'
import scrapAnand from './site-source/anand'
//
import homeCt from './assets/ct/ct_index.json'
import postCt from './assets/ct/ct_post.json'
import scrapCt from './site-source/ct'
//
import homeOl from './assets/ol/ol_index.json'
import postOl from './assets/ol/ol_post.json'
import scrapOl from './site-source/ol'
//
//import homePl from './assets/planet/ChloeAmour/data_ChloeAmourHD.json'
//import {postPl} from './site-source/planet'
//import scrapPl from './site-source/planet'
//
import homePsc from './assets/psc/psc_index.json'
import postPsc from './assets/psc/psc_post.json'
import scrapPsc from './site-source/psc'
//
import homePsi from './assets/psi/most_popular.json'
import postPsi from './assets/psi/most_popular.json'
import scrapPsi from './site-source/psi'
//
import homeSprt from './assets/sprt/todos.json'
//import postSprt from './assets/sprt/sprt_post.json'
import scrapSprt from './site-source/sprt'
//
import homeTec from './assets/tec/tec_index.json'
import postTec from './assets/tec/tec_post.json'
import scrapTec from './site-source/tec'
//
//import homeTf from './assets/tf/tf_index.json'
//import postTf from './assets/tf/tf_post.json'
import scrapTf from './site-source/tf'
//
//import homeTl from './assets/tl/tl_index.json'
//import postTl from './assets/tl/tl_post.json'
import scrapTl from './site-source/tl'
//
//import homeTp from './assets/tp/tp_index.json'
//import postTp from './assets/tp/tp_post.json'
import scrapTp from './site-source/tp'
//
import homeTwk from './assets/twk/twk_index.json'
import postTwk from './assets/twk/twk_post.json'
import scrapTwk from './site-source/twk'
//
import homeWcc from './assets/wcc/wcc_index.json'
import postWcc from './assets/wcc/wcc_post.json'
import scrapWcc from './site-source/wcc'

export default {
    //'planet.com': { dataHome: homePl, scrapper: scrapPl, dataPost: postPl },
    //noticias sobre tecnologia
    'adrenaline.uol.com.br': { dataHome: homeAd, dataPost: postAd, scrapper: scrapAd },
    'anandtech.com': { dataHome: homeAnand, dataPost: postAnand, scrapper: scrapAnand },
    'canaltech.com.br': { dataHome: homeCt, dataPost: postCt, scrapper: scrapCt },
    'olhardigital.com.br': { dataHome: homeOl, dataPost: postOl, scrapper: scrapOl },
    'tecnoblog.net': { dataHome: homeTec, dataPost: postTec, scrapper: scrapTec },
    'wccftech.com': { dataHome: homeWcc, dataPost: postWcc, scrapper: scrapWcc },
    //download
    'ps4iso.net': { dataHome: homePsi, dataPost: postPsi, scrapper: scrapPsi },
    'thepiratefilmes.biz': { dataHome: homePsi, dataPost: postPsi, scrapper: scrapTp },
    'torlock.com': { dataHome: homePsi, dataPost: postPsi, scrapper: scrapTl },
    //forum
    'forum.adrenaline.uol.com.br': { dataForums: forumAdr, dataHome: homeAdr, dataPost: postAdr, scrapper: scrapAdr },
    //nao existe mais
    'spartagames.net': { dataHome: homeSprt, dataPost: postPsi, scrapper: scrapSprt },
    'theps4brasil.club': { dataHome: homePsc, dataPost: postPsc, scrapper: scrapPsc },
    'turbofilmesonline.club': { dataHome: homePsc, dataPost: postPsc, scrapper: scrapPsc },
    //problema ao carregar pagina
    //home
    'torrentfilmes.net': { dataHome: homePsi, dataPost: postPsi, scrapper: scrapTf },
    //post
    'tweaktown.com': { dataHome: homeTwk, dataPost: postTwk, scrapper: scrapTwk },   
}

const sources = [
    {name: 'adrenaline.uol.com.br', type: 'news'},
    {name: 'anandtech.com', type: 'news'},
    {name: 'canaltech.com.br', type: 'news'},
    {name: 'forum.adrenaline.uol.com.br', type: 'forum'},
    {name: 'olhardigital.com.br', type: 'news'},
    {name: 'planet.com', type: 'lenny'},
    {name: 'ps4iso.net', type: 'download'},
    {name: 'tecnoblog.net', type: 'news'},
    {name: 'thepiratefilmes.biz', type: 'download'},
    {name: 'spartagames.net', type: 'download'},
    {name: 'theps4brasil.club', type: 'download'},
    {name: 'torlock.com', type: 'download'},
    {name: 'torrentfilmes.net', type: 'download'},
    {name: 'turbofilmesonline.club', type: 'download'},
    {name: 'tweaktown.com', type: 'news'},
    {name: 'wccftech.com', type: 'news'}
]

export {sources}
