/*
 * Engloba os scrappers e assets
 * De forma que estejam disponiveis para linha de comando utilizando Node.js
 */

import ad from './site-source/ad'
import adr from './site-source/adr'
import anand from './site-source/anand'
import ct from './site-source/ct'
import gn from './site-source/gn'
import ol from './site-source/ol'
import prmc from './site-source/prmc'
import psc from './site-source/psc'
//import planet from './site-source/planet'
import psi from './site-source/psi'
import sprt from './site-source/sprt'
import tec from './site-source/tec'
import tf from './site-source/tf'
import tl from './site-source/tl'
import tp from './site-source/tp'
import twk from './site-source/twk'
import wcc from './site-source/wcc'
import wvd from './site-source/wvd'

const scrappersAvailable = {
    ad, adr, anand, ct, gn, ol, prmc, psc, /*planet,*/ psi, sprt, tec, tf, tl, tp, twk, wcc, wvd
}

// node App.js adr threads 441415 6469
async function call() {
    global.libRequest = 'node-fetch'
    global.libHTMLparse = 'jsdom'

    const path = process.mainModule.paths[0].split('node_modules')[0]
    const i = new Index()

    
    if (typeof process.argv[3] !== 'undefined' && isNaN(process.argv[3])) {//console.log('if')

        if (process.argv[3] === 'postMerge') {
            
            //for (let pagina = 1; pagina <= 9; pagina++) {
            //    console.log(`chamada disparada: ${process.argv[3]}(${process.argv.slice(4).join(',')})`)
            //    const res = await i[process.argv[3]](pagina)
            //
            //    console.log(`chamada disparada: ${process.argv[3]}Dump(objetoRetornado, ${path})`)
            //    i[process.argv[3] + 'Dump'](res, path, pagina)
            //}            

            //const res = await i[process.argv[3]](path, process.argv.slice(4))
        }

        if (typeof i[process.argv[3]] === 'function') {
            //console.log(`${process.argv[ 4 ]} eh um metodo`)
            //console.log(process.argv.slice(4))

            console.log(`chamada disparada: ${process.argv[3]}(${process.argv.slice(4).join(',')})`)
            const res = await i[process.argv[3]](...process.argv.slice(4))

            /*for(let page = 1; page <= 30; page++) {
                console.log('args', process.argv[ 3 ])
                await i[ process.argv[ 3 ] ](
                `http://www.planetsuzy.org/t359480-p${page}-jenna-haze.html`, 
                page, 
                'JennaHazePHOTOS'
                )
            }*/

            console.log(`chamada disparada: ${process.argv[3]}Dump(objetoRetornado, ${path})`)
            i[process.argv[3] + 'Dump'](res, path)
        } else if (process.argv[3] === 'homeListPost') {
            console.log(`${site}: requisitando (pagina inicial) + (postagens)`)
            global.res = await i['home'](...process.argv.slice(4))
            console.log(`Dump da home: homeDump(objetoRetornado, ${path})`)
            const dump = await i['homeDump'](res, path)
            
            console.log('\n\n')
            //res.listPost.forEach(post => {
            for(let key=0; key<res.listPost.length; key++){
                const post = global.res.listPost[key]
                console.log(`...Requisitando postagem (${key+1}/${res.listPost.length}): ${post.link}`)

                try{
                const res = await i['post'](post.link)
                console.log('Dump da postagem')
                await i['postDump'](res, path)
                }catch(e){
                    console.log('ERRO!')
                }
                
                
                console.log('..............................\n\n')
            }
            console.log(`${site}: requisicao finalizada`)
        }
        
    } else {//console.log('else')
        console.log(`Nenhum metodo/pagina associado ao parametro: ${process.argv[4]}`)
    }
}

const site = process.argv[2]
//console.log(site)
const Index = scrappersAvailable[site]
if (typeof Index === null) {
    console.log(`Nenhum scrapper associado ao parametro: ${process.argv[site]}`)
} else {
    call()
}
